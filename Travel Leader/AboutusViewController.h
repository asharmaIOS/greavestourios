//
//  AboutusViewController.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/18/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutusViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *aboutlbl;
@property (weak, nonatomic) IBOutlet UITextView * aboutUsTextview;

@end

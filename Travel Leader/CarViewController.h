//
//  CarViewController.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarViewController : UIViewController
- (IBAction)Limobtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *picktimelb;
@property (weak, nonatomic) IBOutlet UILabel *pickctylb;

@property (weak, nonatomic) IBOutlet UILabel *pickdatelb;
@property (weak, nonatomic) IBOutlet UILabel *droptimelb;
@property (weak, nonatomic) IBOutlet UILabel *dropctylb;
@property (weak, nonatomic) IBOutlet UILabel *dropdatelb;
@property (weak, nonatomic) IBOutlet UILabel *rentbylb;
@property (weak, nonatomic) IBOutlet UILabel *cartypelb;
@property (weak, nonatomic) IBOutlet UILabel *confirmationlb;
@property (weak, nonatomic) NSString *typeUserStr;

@property (weak, nonatomic) UIImage *headerImage;


@end

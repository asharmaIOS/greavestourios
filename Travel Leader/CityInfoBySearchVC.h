//
//  CityInfoBySearchVC.h
//  Travel Leader
//
//  Created by Abhishek Sharma on 1/2/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityInfoBySearchVC : UIViewController


@property (strong, nonatomic)  NSString *strCity;

@end

//
//  CityinfomationVC.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 9/19/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "ViewController.h"

@interface CityinfomationVC : ViewController
@property (nonatomic, assign) BOOL isHomeData;



@property (weak, nonatomic)  NSString *strCity;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *subview;

@end

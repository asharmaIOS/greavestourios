//
//  CityinfomationVC.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 9/19/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "CityinfomationVC.h"
#import "CityDetailCell.h"
#import "SideSeenImageCell.h"
#import "SearchDestinationVC.h"
#import "TravelConstantClass.h"
#import "SideSeenCell.h"
#import "HorizantalCell.h"
#import "XMLReader.h"
#import "SVProgressHUD.h"
#import "UIFont+Extras.h"
#import "ReadMoreVC.h"
@interface CityinfomationVC ()

{
    int selectedcity;
    __weak IBOutlet UIImageView *imageviewMain;
    
    __weak IBOutlet UICollectionView *horizantalCollectionview;
    
    __weak IBOutlet NSLayoutConstraint *consHeightCollectionview;
    __weak IBOutlet UICollectionView *collectioview;
    __weak IBOutlet UILabel *labelContent;
   
    NSMutableArray * citydescriptionarr;
    NSMutableArray*cityname;
    NSMutableArray*cityimge;
    NSMutableArray*citysideseenarr;
    NSString*sidedetail;
    NSString * image;
    NSString*city;
    NSMutableArray * pointsofinterestarr;
   
}

@property (weak, nonatomic) IBOutlet UILabel *citynamelbl;

@end

@implementation CityinfomationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    citydescriptionarr = [[NSMutableArray alloc]init];
    cityname=[[NSMutableArray alloc]init];
    cityimge=[[NSMutableArray alloc]init];
    citysideseenarr=[[NSMutableArray alloc]init];
    pointsofinterestarr = [[NSMutableArray alloc]init];
    
    [self setupLeftMenuButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    //  [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    [self loadDataAccordingCity];
    
}
#pragma mark - navigation bar
-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //Text
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    textButton.title = @"";
  
    textButton.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton];
    

}
-(void)backButtonTapp:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)loadDataAccordingCity
{
        _citynamelbl.text = _strCity;
       _strCity = [_strCity stringByReplacingOccurrencesOfString:@" " withString:@""];


        [SVProgressHUD showWithStatus:@"" maskType:SVProgressHUDMaskTypeBlack];
        NSString *stringUrl = [NSString stringWithFormat:@"http://api.arrivalguides.com/api/xml/Travelguide?auth=057bbab0770837e2364596fb21d6cf0e35ac9e44&lang=en&limit=2&skip=2&v=13&iso=%@",_strCity];
        NSURL *url = [NSURL URLWithString:stringUrl];
        
        NSData *xmlData = [[NSData alloc] initWithContentsOfURL:url];
        NSError *parseError = nil;
        NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:xmlData error:&parseError];
        NSLog(@"%@", xmlDictionary);
        
        NSArray *sectionarray=[[[xmlDictionary objectForKey:@"destination"]objectForKey:@"sections"]objectForKey:@"section"];
        NSString *cityimage=[[[xmlDictionary objectForKey:@"destination"]objectForKey:@"icon"]objectForKey:@"url"];
        
       // imageviewMain.image = [UIImage imageNamed:@"HongKong3x.png"];
        
        
        NSURL* imageurl = [NSURL URLWithString:cityimage];
        NSLog(@"url%@",url);
        //   cell.imageview.layer.cornerRadius = 10;
        
        NSData   *data = [NSData dataWithContentsOfURL:imageurl];
        imageviewMain.image = [UIImage imageWithData:data];
        
        [citydescriptionarr removeAllObjects];
        [cityimge removeAllObjects];
        
        for (int i =0; i < sectionarray.count; i++) {
            NSDictionary *dictsection = [sectionarray objectAtIndex:i];
            
            if ([dictsection valueForKey:@"description"] != nil) {
                [citydescriptionarr addObject:[dictsection valueForKey:@"description"]];
                
            }
            else{
                [citydescriptionarr addObject:@""];
            }
            [cityname addObject:[dictsection valueForKey:@"name"]];
            
            
            
            
            if ([[dictsection valueForKey:@"pointsofinterest"] valueForKey:@"entry"] != nil) {
                if ([[[dictsection valueForKey:@"pointsofinterest"] valueForKey:@"entry"] isKindOfClass:[NSDictionary class]]) {
                    NSMutableArray *arrsub = [[NSMutableArray alloc]initWithObjects:[[dictsection valueForKey:@"pointsofinterest"] valueForKey:@"entry"], nil];
                    [cityimge addObject:arrsub];
                }
                else{
                    NSArray *arr = [[dictsection valueForKey:@"pointsofinterest"] valueForKey:@"entry"];
                    NSMutableArray *objimages = [[NSMutableArray alloc]initWithArray:arr];
                    
                    [cityimge addObject:objimages];
                }
                
            }
            else{
                [cityimge addObject:[[NSMutableArray alloc]init]];
            }
            
            
        }
        [self selected];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            
            if ([[NSString stringWithFormat:@"%@", [[[xmlDictionary valueForKey:@"Errors"] valueForKey:@"ErrorDescription"] objectAtIndex:0]] isEqualToString:@"No Data Found!"]) {
                //xmlDictionary = nil;
            }
            // [self.triplistTabel reloadData];
            
        });
        
 
}



-(void)selected
{
    labelContent.text = [citydescriptionarr objectAtIndex:0];
    
    selectedcity = 0;
    consHeightCollectionview.constant = 0;
    _subview.hidden = YES;
    
    
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-20, FLT_MAX);
    
    CGSize expectedLabelSize = [labelContent.text sizeWithFont:labelContent.font constrainedToSize:maximumLabelSize lineBreakMode:labelContent.lineBreakMode];
    
    _scrollview.contentSize = CGSizeMake(self.view.frame.size.width, 285+expectedLabelSize.height+consHeightCollectionview.constant+50 );
}


#pragma mark - UICollectionViewDataSource methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (collectionView == horizantalCollectionview) {
        return [cityname count];
        
        
    }
    else{
        NSMutableArray *arr = [cityimge objectAtIndex:selectedcity];
        return  [arr count];
        
    }
    
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // UICollectionViewCell * customCell = nil;
    
    
    
    if (collectionView == horizantalCollectionview)
    {
        
        HorizantalCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HorizantalCell" forIndexPath:indexPath];
        cell.labelName.text = [NSString stringWithFormat:@"%@",[cityname objectAtIndex:indexPath.row]];
       
       // cell.labelName.font = [UIFont setFontWithType:@"Raleway-Bold" withSize:7];
      
        
        cell.labelName.highlightedTextColor = [UIColor orangeColor];
        [cell.labelName sizeToFit];
        return cell;
    }
    
    else
    {
        
        SideSeenImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SideSeenImageCell" forIndexPath:indexPath];
        NSMutableArray *arr = [cityimge objectAtIndex:selectedcity];
        
        cell.labelContent.text = [[arr objectAtIndex:indexPath.row] valueForKey:@"description"];
        cell.labelContent.highlightedTextColor = [UIColor blackColor];
        cell.labelHeader.text =[NSString stringWithFormat:@"%@",[[arr objectAtIndex:indexPath.row] objectForKey:@"title"]];
        cell.labelHeader.highlightedTextColor = [UIColor blackColor];
        cell.labelContent.textAlignment = NSTextAlignmentLeft;
        cell.labelContent.layer.cornerRadius = 7;
        cell.labelContent.layer.masksToBounds = YES;
        cell.labelContent.layer.borderWidth = 0; //
        cell.imageview.layer.cornerRadius = 7;
        cell.imageview.layer.masksToBounds = YES;
        cell.imageview.layer.borderWidth = 0; //
        cell.labelHeader.layer.cornerRadius =7;
        cell.labelHeader.layer.masksToBounds = YES;
        cell.labelHeader.layer.borderWidth = 0; //
       
        NSMutableDictionary * Dicimage = [arr valueForKey:@"images"];
        NSDictionary *dictsub = [Dicimage valueForKey:@"image"];
        
        
        NSMutableArray *imagearrr = [dictsub valueForKey:@"url"];
        
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[imagearrr objectAtIndex:indexPath.row]]];
        NSLog(@"url%@",url);
        //   cell.imageview.layer.cornerRadius = 10;
        
        NSData   *data = [NSData dataWithContentsOfURL:url];
        cell.imageview.image = [UIImage imageWithData:data];
        
        NSLog(@"imge%@",cell.imageview.image);
        
        
        return cell;
      
        
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == horizantalCollectionview) {
        
        selectedcity = indexPath.row;
        labelContent.text = [citydescriptionarr objectAtIndex:indexPath.row];
        if ([[cityimge objectAtIndex:selectedcity] count] == 0) {
            _subview.hidden = YES;
            consHeightCollectionview.constant = 0;
            
        } else {
            _subview.hidden = NO;
            
            consHeightCollectionview.constant = 331;
        }
        
        CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-20, FLT_MAX);
        
        CGSize expectedLabelSize = [labelContent.text sizeWithFont:labelContent.font constrainedToSize:maximumLabelSize lineBreakMode:labelContent.lineBreakMode];
        
        _scrollview.contentSize = CGSizeMake(self.view.frame.size.width, 285-21+expectedLabelSize.height+consHeightCollectionview.constant );
        [collectioview reloadData];
    }
    else{
        NSMutableArray *arr=[cityimge objectAtIndex:selectedcity];
        NSDictionary *dict  = [arr objectAtIndex:indexPath.row];
        
        NSMutableDictionary * Dicimage = [dict valueForKey:@"images"];
        NSDictionary *dictsub = [Dicimage valueForKey:@"image"];
        
        
       [TravelConstantClass singleton].url = [dictsub valueForKey:@"url"];
        NSLog(@"selct%@",[TravelConstantClass singleton].url);
        
        [TravelConstantClass singleton].descriptions = [dict valueForKey:@"description"];
        [TravelConstantClass singleton].phone = [dict valueForKey:@"meta"];
       // [TravelConstantClass singleton].url = [dict valueForKey:@"url"];
        [TravelConstantClass singleton].email = [dict valueForKey:@"meta"];
        [TravelConstantClass singleton].address = [dict valueForKey:@"meta"];
        
        
        ReadMoreVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ReadMoreVC"];
        [self.navigationController pushViewController:vc animated:YES];
        // selection for below collectionview
    }
    
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    if (collectionView == collectioview) {
        
        return CGSizeMake(self.view.frame.size.width-60, 261);
        
    }
    else if (collectionView == horizantalCollectionview)
    {
        
         return CGSizeMake(120, 30);
    }
        
    else
    {
        return CGSizeMake(0, 0);
       // return CGSizeMake(100, 25);
    }
    
}




@end

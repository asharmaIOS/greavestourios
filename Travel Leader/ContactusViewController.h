//
//  ContactusViewController.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/18/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactusViewController : UIViewController <UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *contactlbl;
@property (weak, nonatomic) IBOutlet UILabel *emaillbl;
@property (weak, nonatomic) IBOutlet UILabel *websitelbl;
@property (weak, nonatomic) IBOutlet UILabel *afterhourcontct;
@property (weak, nonatomic) IBOutlet UILabel *afterhoutlb;
@property (weak, nonatomic) IBOutlet UILabel *phonelb;
@property (weak, nonatomic) IBOutlet UIImageView *phoneimage;
@property (weak, nonatomic) IBOutlet UIImageView *phoneicon;


- (IBAction)contactbtn:(id)sender;

- (IBAction)mailbtn:(id)sender;

@end

//
//  TripViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/19/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "FlightDetailsVC.h"
#import "TripCell.h"
#import <Foundation/Foundation.h>
#import "TravelConstantClass.h"
#import "SVProgressHUD.h"
#import "FlightViewController.h"
#import "CarViewController.h"
#import "HomeViewController.h"
#import "ValidationAndJsonVC.h"
#import "Reachability.h"
#import "NotifDetailsVC.h"
#import "XMLReader.h"
#import "CityCell.h"
#import "WebCheckInVC.h"
#import "HotelViewController.h"
#import "DemoMessagesViewController.h"


#import "CityInfoBySearchVC.h"

#import "SearchDestinationVC.h"

#import "ReadMoreDescriptionOfdestinationViewController.h"



#import "NotificationCell.h"
#import <GooglePlaces/GooglePlaces.h>




@interface FlightDetailsVC ()  <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchControllerDelegate,UISearchResultsUpdating>
{
    NSArray *filterrr;
    
    NSMutableArray *sectionarr;
    
    NSMutableArray * dataarr;
    UIImage * headerImg;
    
    UIScrollView *scrollViewForSegment;
    NSArray *completeDataArray;
    UIScrollView *scrollViewForSepcificData;
    NSString *strCityNameWithState;
    NSString *guidelineUrlString;
    
    NSMutableArray* cityarr;
    NSMutableArray* webAddressArray;
    UIWebView *_webView;
    
    NSMutableArray *dataArrayAccordingdate;

    NSString *responseType;
    
    NSMutableArray* dateArray;
    NSMutableArray* cityarry;
    NSMutableArray* segmentArrayOfButtons;
    NSArray *finalData;
    NSArray *fillerarr;
    NSArray *arrayOfarray;
    NSMutableArray *imageOfCityArr ;
    
    
}

@property (weak, nonatomic) IBOutlet UIView *searchbar;
@property (weak, nonatomic) IBOutlet UILabel *popularLbl;
@property (weak, nonatomic) IBOutlet UILabel *underlineLbl;

@property (weak, nonatomic) IBOutlet UITableView *citytabel;
@property (nonatomic, strong) NSMutableArray *searchResult;
@property (weak, nonatomic) UISegmentedControl *segmentController;
@property (weak, nonatomic) IBOutlet UILabel *citynamelbl;

@property (weak, nonatomic) IBOutlet UITableView *mytriptbl;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UIView *emptyview;
@property (weak, nonatomic) IBOutlet UIView *searchCityView;
@property (nonatomic, strong) UISearchController *searchController;




@property (weak, nonatomic) IBOutlet UITableView *notificationtbl;

@end

@implementation FlightDetailsVC
@synthesize listArray,dataDic,destinationString,NotificationArray,emptyview;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.emptyview.hidden = YES;
    responseType = @"TL24";
    // Do any additional setup after loading the view.
    filterrr =  [[ NSArray alloc]init];
    sectionarr = [[NSMutableArray alloc]init];
    dataarr = [[NSMutableArray alloc]init];
    NotificationArray = [[NSMutableArray alloc]init];
    cityarr = [[NSMutableArray alloc]init];
    cityarry = [[NSMutableArray alloc]init];
    
    dataArrayAccordingdate = [[NSMutableArray alloc]init];
    dateArray = [[NSMutableArray alloc]init];
    segmentArrayOfButtons = [[NSMutableArray alloc]init];
    webAddressArray = [[NSMutableArray alloc]init];
    imageOfCityArr = [[NSMutableArray alloc]init];
    
    // Search bar controller
    
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];

     // Use the current view controller to update the search results.
      //_searchController.searchResultsUpdater = self;
     self.searchController.hidesNavigationBarDuringPresentation = false;
    // _searchBgView.frame = CGRectMake(50, 165,screenWidth-100 , 40);
     self.searchController.searchBar.frame = CGRectMake(0,0, _searchbar.frame.size.width, 48.0);
     [_searchbar addSubview:[_searchController searchBar]];
     _searchController.obscuresBackgroundDuringPresentation = false; // The default is true.

    // self.citytabel.tableHeaderView = self.searchController.searchBar;
     //_searchController.searchBar.searchTextField.backgroundColor = [UIColor clearColor];
    // _searchController.searchBar.barTintColor = [UIColor clearColor];
     _searchController.delegate = self;
   //  _searchController.searchBar.searchTextField.placeholder = @"Explore Destination";
     _searchController.searchBar.delegate = self;
     self.definesPresentationContext = YES;

    
    
    
    
    [cityarry addObject:@"LONDON"];
    [cityarry addObject:@"NEW YORK"];
    [cityarry addObject:@"PARIS"];
    [cityarry addObject:@"LISBON"];
    [cityarry addObject:@"LOS ANGELES"];
    [cityarry addObject:@"HONG KONG"];
    
    [imageOfCityArr addObject:@"london.png"];
    [imageOfCityArr addObject:@"NewYork.png"];
    [imageOfCityArr addObject:@"Paris.png"];
    [imageOfCityArr addObject:@"Lisbon.png"];
    [imageOfCityArr addObject:@"LosAngeles.png"];
    [imageOfCityArr addObject:@"HongKong.png"];
    
    NSArray *segmentArray = [[NSArray alloc]initWithObjects:@"Itinerary",@"Alerts",@"Destination Guide", nil];
    
    
    [self drawSegmentController:segmentArray];
    
   // [self GETCity ];

    
    
////    [[UISegmentedControl appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor orangeColor] } forState:UIControlStateSelected];
////    
////    // color disabled text ---> blue
////    [[UISegmentedControl appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateNormal];
//    
//    
//    [[UISegmentedControl appearance] setSelectedSegmentIndex:0];
    // color tint segmented control ---> Your color
    
  //    [[UISegmentedControl appearance] s];
 //   [[UISegmentedControl appearance] setTintColor:[UIColor blackColor]];
    
    
    
    
    

    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    self.mytriptbl.tableFooterView = [UIView new];
    self.notificationtbl.tableFooterView = [UIView new];

    
    NSLog(@"lastdictionary data%@",dataDic);
    
    if (networkStatus == NotReachable) {
        
       
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        NSArray *dataArray  = [userDef objectForKey:@"TripList"];
        [userDef synchronize];
        
        for (int i = 0; i < dataArray.count ; i++) {
            
            
            if ([[[dataArray objectAtIndex:i]valueForKey:@"destination"]isEqualToString:[dataDic valueForKey:@"destination"]] && [[[dataArray objectAtIndex:i]valueForKey:@"itinstart"]isEqualToString:[dataDic valueForKey:@"itinstart"]]) {
                
                listArray = [[dataArray objectAtIndex:i]valueForKey:@"date"];
                filterrr =  [listArray valueForKey:@"formatteddate"];
                
                NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:filterrr];
                filterrr = [[NSMutableArray alloc] initWithArray:[mySet array]];
                
                [self.mytriptbl reloadData];

            }
        }
        
        
    }
    
    else {
          [self chatGetPNRApi];
        
        [GMSPlacesClient provideAPIKey:@"AIzaSyAK7edZ2truD9B04TX_gwIA0bduJ0OzTeg"];
        // Do any additional setup after loading the view, typically from a nib.

    }
    
    
  
    [self setupLeftMenuButton];
    
//    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
//    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
//    textButton.title = @"TRIPS";
//    // textButton.tintColor = [UIColor whiteColor];
//    textButton.tintColor = [UIColor whiteColor];
//    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    
    
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}
    


- (void)viewWillAppear:(BOOL)animated
{
    //  [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
    _citytabel.hidden = YES;
    _searchController.searchBar.text = @"";
    [_searchController.searchBar resignFirstResponder];
    
    [self updateFrameOfAllviews];
    //[self registerForKeyboardNotifications];
    // [self ChatgetPNR];

}



-(void)drawSegmentController:(NSArray *)dataArray {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
   
    
    scrollViewForSegment = [[UIScrollView alloc] init];
    //scrollView.backgroundColor = [UIColor grayColor ];
    scrollViewForSegment.frame = CGRectMake(0, 64, screenWidth , 40);
    scrollViewForSegment.backgroundColor = [UIColor blackColor];
    CGFloat finalWidth = 0;
    
    for (int i = 0; i <3 ; i++) {
        
        CGFloat buttonWidth = screenWidth /3;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.tag = i;
        
        [button addTarget:self
                   action:@selector(segBtnTapped:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[dataArray objectAtIndex:i] forState:UIControlStateNormal];
        
        [button setFrame:CGRectMake(finalWidth,01, buttonWidth , 35)];
        
        if (i==2) {
            
            button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            
            button.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
            [button setTitle: @"Destination\nGuide" forState: UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:15.0];

        }
        
        else {
            
            button.titleLabel.font = [UIFont systemFontOfSize:15];

        }
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        if (i==0) {
            
            [button setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            
        }
        
       
        [scrollViewForSegment addSubview:button];
        [segmentArrayOfButtons addObject:button ];
        finalWidth = finalWidth + button.frame.size.width;
    }
    
    
    scrollViewForSegment.showsHorizontalScrollIndicator = NO;
    
    scrollViewForSegment.contentSize = CGSizeMake(screenWidth, 40);
    [self.view addSubview:scrollViewForSegment];
    
}




-(void)updateFrameOfAllviews {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;

    if (screenHeight >= 812) {
        
        //_segmentController.frame = CGRectMake(0, 90, screenWidth, 35);
        scrollViewForSegment.frame = CGRectMake(0, 88, screenWidth , 40);

        _imageview.frame = CGRectMake(0, 125, screenWidth, 182);
        _mytriptbl.frame = CGRectMake(0, 307, screenWidth, screenHeight-302);
        _notificationtbl.frame = CGRectMake(0, 125, screenWidth, screenHeight-125);
        
        _searchbar.frame =  CGRectMake(0, 129, screenWidth, 50);
        _citytabel.frame =  CGRectMake(0, 179, screenWidth, 200);

    }
    
    else {
    //_segmentController.frame = CGRectMake(0, 64, screenWidth, 35);
    
    scrollViewForSegment.frame = CGRectMake(0, 64, screenWidth , 40);

    
    _imageview.frame = CGRectMake(0, 100, screenWidth, 182);
    _mytriptbl.frame = CGRectMake(0, 282, screenWidth, screenHeight-282);
    _notificationtbl.frame = CGRectMake(0, 100, screenWidth, screenHeight-100);
    _searchbar.frame =  CGRectMake(0, 105, screenWidth, 50);
    _citytabel.frame =  CGRectMake(0, 155, screenWidth, 200);
    
    }
}


- (void)loadFirstPhotoForPlace:(NSString *)placeID {
    [[GMSPlacesClient sharedClient]
     lookUpPhotosForPlaceID:placeID
     callback:^(GMSPlacePhotoMetadataList *_Nullable photos,
                NSError *_Nullable error) {
         if (error) {
             // TODO: handle the error.
             NSLog(@"Error: %@", [error description]);
         } else {
             if (photos.results.count > 0) {
                 GMSPlacePhotoMetadata *firstPhoto = photos.results.lastObject;
                 //GMSPlacePhotoMetadata *firstPhoto = nil;

                 [self loadImageForMetadata:firstPhoto];
                 
             }
         }
     }];
}



- (void)loadImageForMetadata:(GMSPlacePhotoMetadata *)photoMetadata {
    [[GMSPlacesClient sharedClient]
     loadPlacePhoto:photoMetadata
     constrainedToSize:self.imageview.bounds.size
     scale:self.imageview.window.screen.scale
     callback:^(UIImage* _Nullable photo, NSError* _Nullable error) {
         if (error) {
             // TODO: handle the error.
             self.imageview.image = [UIImage imageNamed:@"logo.png"];

            // NSLog(@"Error: %@", [error description]);
         } else {
             
             if(photo) {
             self.imageview.image = photo;
                 headerImg = photo;
             }
             else  {
                 
                  self.imageview.image = [UIImage imageNamed:@"logo.png"];
                 headerImg = [UIImage imageNamed:@"logo.png"];
             }
                 
             //self.attributionTextView.attributedText = photoMetadata.attributions;
         }
     }];
}

#pragma mark - navigation bar
-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //Text
//    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
//    textButton.title = @"";
//    // textButton.tintColor = [UIColor whiteColor];
//    textButton.tintColor = [UIColor blackColor];
//    self.navigationItem.leftBarButtonItems = @[searchItem, textButton];
//
    
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"TRIPS"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
    
    //  self.navigationItem.leftBarButtonItem = @[searchItem];
}


-(void)backAction
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)backButtonTapp:(id)sender {
    
    //    HomeViewController  *vc = [[UIStoryboard storyboardWithName:@"Main.storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"homevc"];
    //    [self.navigationController pushViewController:vc animated:YES];
    // [self dismissModalViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -  Method for GetP


- (void)googleApiFor: (NSString *)destinationStr{
    
    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyAK7edZ2truD9B04TX_gwIA0bduJ0OzTeg",destinationStr ];
    
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *data = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            NSError *erro = nil;
            
            NSString *placeID ;
            if (data!=nil) {
                
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];
                
                if (json.count > 0) {
                    
                    NSLog(@"resultdata == %@",[[[json valueForKey:@"results"]lastObject]valueForKey:@"place_id"]);
                    
                    if([[[json valueForKey:@"results"]lastObject]valueForKey:@"place_id"]) {
                        
                        placeID = [[[json valueForKey:@"results"]lastObject]valueForKey:@"place_id"];
                        
                      
                    }
                    
                }
                    
                
            }
            dispatch_sync(dispatch_get_main_queue(),^{
               
                [self loadFirstPhotoForPlace:placeID];

            
            });
        }];
        
        [data resume];
  
    
}


- (IBAction)segBtnTapped:(id)sender {
    
//    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                [UIFont boldSystemFontOfSize:13], NSFontAttributeName,
//                                [UIColor whiteColor], NSForegroundColorAttributeName,
//                                nil];
//    [_segmentController setTitleTextAttributes:attributes forState:UIControlStateNormal];
//    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:[UIColor orangeColor] forKey:NSForegroundColorAttributeName];
//    [_segmentController setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
//    
//     [_segmentController setTintColor:[UIColor blackColor]];

    
    int index = [sender tag] ;
    
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    
    for (int i = 0; i<segmentArrayOfButtons.count; i++) {
        
        if (i == index) {
            [[segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        }
        else {
            [ [segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
    }
    
    if(index==0){
        _notificationtbl.hidden = YES;
        _mytriptbl.hidden = NO;
        _imageview.hidden = NO;
        self.emptyview.hidden = YES;
        self.notificationtbl.hidden = YES;
        self.searchCityView.hidden = YES;
        _searchController.searchBar.hidden = YES;
        _citytabel.hidden = YES;
         

        [_mytriptbl reloadData];
         [self updateFrameOfAllviews];
        
    }
    else if(index==1){
        _mytriptbl.hidden = YES;
        _imageview.hidden = YES;
        _notificationtbl.hidden = NO;
        self.notificationtbl.hidden = NO;
        self.searchCityView.hidden = YES;
        _searchController.searchBar.hidden = YES;
        _citytabel.hidden = YES;
          

        [self ListAlert];
         [self updateFrameOfAllviews];
        
        
        
    }
    else{
        [self GETCity ];

        [ self loadpopulatCityonScrollview ];

        [self updateFrameOfAllviews];
        
        _citytabel.hidden = YES;
           _searchController.searchBar.text = @"";
           [_searchController.searchBar resignFirstResponder];
        
        _searchController.searchBar.hidden = NO;

        self.searchCityView.hidden = NO;
        _imageview.hidden = YES;

        self.emptyview.hidden = YES;
        self.notificationtbl.hidden = YES;
        _mytriptbl.hidden = YES;
       
      

        
        
        
    }
    
   
    
   // [[_segmentController.subviews objectAtIndex:_segmentController.selectedSegmentIndex] setTintColor: [UIColor orangeColor]];
//    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor orangeColor]} forState:UIControlStateSelected];
    
//    [_segmentController setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial" size:16.0],
//                                           NSForegroundColorAttributeName:[UIColor redColor],
//                                           NSShadowAttributeName:shadow}
//                                forState:UIControlStateNormal];

}

- (void)chatGetPNRApi {
    
    [SVProgressHUD showWithStatus:@"Trips.. " maskType:SVProgressHUDMaskTypeBlack];
    
   NSDictionary *dictionary = @{@"pkey":[TravelConstantClass singleton].pkey};
    
//    NSDictionary *dictionary = @{@"pkey":@"3A10A18BACC645CDA7E38B8227A604A3"};


    NSString *urlStr = @"ChatGetPNR";
    NSDictionary *dataDic = [ValidationAndJsonVC getParsingData:dictionary GetUrl:urlStr GetTimeinterval:30];
    
    
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
        
        listArray = [[dataDic valueForKey :@"pnr"]valueForKey:@"date"];
        
        for (int i=0; i<listArray.count; i++) {
            
            [dateArray addObject:[[listArray objectAtIndex:i]valueForKey:@"formatteddate"]];
            [dataArrayAccordingdate addObject:[[listArray objectAtIndex:i]valueForKey:@"travelsegment"]];
            
          
        }
        
        for (int i =0; i<dataArrayAccordingdate.count; i++) {
            
            if ([[dataArrayAccordingdate objectAtIndex:i] count ] >1) {
                
                responseType = @"safeharbor";
            }
            
        }
        
        
        
        NSLog(@"dateArray==%d  dataarraye....%d",dateArray.count,dataArrayAccordingdate.count);
        
        
         filterrr =  [listArray valueForKey:@"formatteddate"];
//        arrayOfarray = [listArray valueForKey:@"formatteddate"];
        NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:filterrr];
        filterrr = [[NSMutableArray alloc] initWithArray:[mySet array]];
        
        NSLog(@"filterarr%@",filterrr);
        
        
            
        
        
        
      //  NSString *strSpaceRemove = [self.destinationString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *strSpaceRemove = [self.destinationString stringByReplacingOccurrencesOfString:@" "
                                       withString:@""];
        
      [self googleApiFor:strSpaceRemove];

        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (listArray  == nil)
                
            {
                [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];

            }
            [self.mytriptbl reloadData];
            
        });

    }
    
}


-(void)ListAlert
{
    
    [SVProgressHUD showWithStatus:@"Your Trips.." maskType:SVProgressHUDMaskTypeBlack];
    // self.emptyview.hidden = YES;
    
    NSDictionary *dictionary = @{@"travelerID":[TravelConstantClass singleton].travelerID};
    
    NSString *urlStr = @"ListAlerts";
    NSDictionary *dataDic1 = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    
    
    
    if ( dataDic1.allKeys == nil) {
        
        // self.emptyview.hidden = NO;
        // self.NoTriplbl.text = @"You Dont Have Any Upcoming Trip Yet";
        // NSLog(@"pnr%@",listArray);
        
        //self.emptyview.hidden = NO;
        
        [SVProgressHUD dismiss];
        
    }
    
    [NotificationArray removeAllObjects];
    
    if (dataDic.allKeys != nil) {
        // [SVProgressHUD dismiss];
        
    NSArray *notificationarr = [dataDic1 valueForKey :@"data"];
        
        for (int i=0; i< notificationarr.count; i++) {

           
                
                if ([[dataDic valueForKey:@"recordlocator"] isEqualToString:[[[[notificationarr objectAtIndex:i]valueForKey:@"detail"]valueForKey: @"flight"] valueForKey:@"airlineConfirmationCode"]]) {
                    
                    
                    [NotificationArray  addObject:[notificationarr objectAtIndex:i]];

                }


            }

        }
//
        
        
        
      
        
        if (NotificationArray.count ==0) {
            
            // self.emptyview.hidden = NO;
            // self.NoTriplbl.text = @"No notifications found!";
            self.emptyview.hidden = NO;
            self.notificationtbl.hidden = YES;
            [SVProgressHUD dismiss];
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^
        {
            
            [self.notificationtbl reloadData];
            
        });
        
        
    
}




-(void)webCheckBtnAction:(UIButton*)sender
{
    int section = sender.tag / 1000;
    int row = sender.tag % 1000;
    
    NSLog(@" section = %d  row = %d",section, row);
    
    NSString *str = [filterrr objectAtIndex:section];
    NSMutableArray *arrRows = [[NSMutableArray alloc]init];
    for (int i =0 ;i < [listArray count]; i++)
    {
        NSDictionary *dictin = [listArray objectAtIndex:i];
        NSString *dictsub = [dictin valueForKey:@"formatteddate"];
        
        if ([str isEqualToString:dictsub]) {
            [arrRows addObject:[listArray objectAtIndex:i]];
        }
    }
    
    //    webAddressArray =  [arrRows mutableCopy];
    
    NSDictionary *testDict = arrRows[row];
    
    
    for (NSDictionary *dictsub in [testDict valueForKey:@"travelsegment"])
    {
        
        if ([dictsub valueForKey:@"airseg"]!= nil)
        {
            
            
            
  NSString *webAddress = [[dictsub valueForKey:@"airseg"]valueForKey:@"webaddress"];
            
            WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
            myView.webLink = webAddress;
            
            [self.navigationController pushViewController:myView animated:true];
            
            
            
        }
    }
    

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _mytriptbl) {
        
        if ([responseType isEqualToString:@"TL24"]) {
            
            return filterrr.count;
        }
        
        else {
            return dateArray.count;  }
    }
    else
    {
         return 1;
        
    }
 
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
//    if (tableView == _mytriptbl) {
//        NSString *str = [filterrr objectAtIndex:section];
//        NSMutableArray *arrRows = [[NSMutableArray alloc]init];
//        for (int i =0 ;i < [listArray count]; i++)
//        {
//            NSDictionary *dictin = [listArray objectAtIndex:i];
//            NSString *dictsub = [dictin valueForKey:@"formatteddate"];
//
//            if ([str isEqualToString:dictsub]) {
//                [arrRows addObject:[listArray objectAtIndex:i]];
//            }
//        }
        
        if (tableView == _mytriptbl) {
          
          if ([responseType isEqualToString:@"TL24"]) {
              
                    NSString *str = [filterrr objectAtIndex:section];
              NSMutableArray *arrRows = [[NSMutableArray alloc]init];
              for (int i =0 ;i < [listArray count]; i++)
              {
                  NSDictionary *dictin = [listArray objectAtIndex:i];
                  NSString *dictsub = [dictin valueForKey:@"formatteddate"];

                  if ([str isEqualToString:dictsub]) {
                      [arrRows addObject:[listArray objectAtIndex:i]];
                  }
              }

              
              return arrRows.count;
          }
        
          else {
              
              NSArray *rowcount = [dataArrayAccordingdate objectAtIndex:section];
                    return rowcount.count ;
          }
            
        
        
        
//        NSArray *rowcount = [dataArrayAccordingdate objectAtIndex:section];
//        return rowcount.count ;
       // return arrRows.count;
        
    }
    else if (tableView == _citytabel) {

         return cityarr.count;
    }
    else
    {
        return NotificationArray.count;
        
    }
    
 
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    
    return 25;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 //- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
   // NSString *headerTitle = filterrr[section];
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 19)];

    if (tableView == _mytriptbl) {
    

    // 1. The view for the header
   // UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 19)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    // 3. Add a label
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(15, 2, tableView.frame.size.width-10 , 20);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
   // headerLabel.font = [UIFont  fontWithName:@"lato" size:10];
    headerLabel.font = [UIFont systemFontOfSize:10] ;
    NSString *testDict = filterrr[section];
//    NSLog(@"arr%@",listArray);
//    NSLog(@"dic%@",testDict);
   // headerLabel.text = [testDict valueForKey:@"formatteddate"];
        
        NSString *tctString;
        if (tableView == _mytriptbl) {
              
              if ([responseType isEqualToString:@"TL24"]) {
                  
                  tctString = testDict;
                  

              }
            
              else {
                  
                  tctString = dateArray[section];

              }
        }
   // tctString = dateArray[section];
    headerLabel.text = tctString.uppercaseString;
    NSLog(@"123654%@",headerLabel.text);

    [headerView addSubview:headerLabel];
    
        
    }
    
    else {
        
        
    }
    return headerView;
}

//- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UIImage *background = nil;
//    background = [UIImage imageNamed:@"grey-bg-3x.png"];
//    return background;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TripCell";
    
    TripCell *cell = (TripCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableView == _mytriptbl)
    {
        if ([responseType isEqualToString:@"TL24"]) {
     
      static NSString *CellIdentifier = @"TripCell";

      TripCell *cell = (TripCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
      if (cell == nil) {
      cell = [[TripCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
      }
     

      NSString *str = [filterrr objectAtIndex:indexPath.section];
      NSMutableArray *arrRows = [[NSMutableArray alloc]init];
      for (int i =0 ;i < [listArray count]; i++)
      {
      NSDictionary *dictin = [listArray objectAtIndex:i];
      NSString *str2 = [dictin valueForKey:@"formatteddate"];

      if ([str isEqualToString:str2]) {
      [arrRows addObject:[listArray objectAtIndex:i]];
      }
      }

      cell.webCheckInBtn.hidden = YES;

      NSDictionary *testDict = arrRows[indexPath.row];


      for (NSDictionary *dictsub in [testDict valueForKey:@"travelsegment"])
      {

      if ([dictsub valueForKey:@"airseg"]!= nil)
      {


      // cell.uparlineimageview.hidden = YES;
      // cell.lowerlineimgeview.hidden = NO;
      cell.timeLbl.text = [[dictsub valueForKey:@"airseg"] valueForKey:@"departuretime"];
      NSString *timeStr = [[dictsub valueForKey:@"airseg"] valueForKey:@"departuretime"];
      NSString *date = [[dictsub valueForKey:@"airseg"] valueForKey:@"departuredate"];

      // code for weblink Show /Hide

      NSArray *dateArrayObjects = [date componentsSeparatedByString:@","];
      NSString *dateStr = [dateArrayObjects lastObject];

      NSString *finaldateStr = [NSString stringWithFormat:@"%@ %@",dateStr,timeStr];


      NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
      [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];

      NSDate *currentDate = [NSDate date];
      NSString *dateString = [formatter stringFromDate:currentDate];

      NSLog(@" datestr =%@",dateString);



      NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
      [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
      NSDate *date1 = [dateFormatter dateFromString:finaldateStr];
      NSDate *date2 = [dateFormatter dateFromString:dateString];

      NSTimeInterval distanceBetweenDates = [date1 timeIntervalSinceDate :date2 ];
      double secondsInAnHour = 3600;
      NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;

      NSLog(@"hoursBetweenDates: %ld", hoursBetweenDates);


      //if ((hoursBetweenDates <= 24) || ( hoursBetweenDates >= -24)) {

      if ( 0 < hoursBetweenDates && hoursBetweenDates <= 24) {


      cell.webCheckInBtn.hidden = NO;

      }



      [cell.webCheckInBtn addTarget:self action:@selector(webCheckBtnAction:) forControlEvents:UIControlEventTouchUpInside];
      cell.webCheckInBtn.tag = (indexPath.section * 1000) + indexPath.row;



      NSString * flightno =[NSString stringWithFormat:@" %@",[[dictsub valueForKey:@"airseg"]valueForKey:@"flightnumber"]];

      NSString *upperCaseStr;

      if ([[[dictsub valueForKey:@"airseg"]valueForKey:@"fromcity"] isKindOfClass:[NSArray class]])
      {
      NSArray *array = [[dictsub valueForKey:@"airseg"]valueForKey:@"fromcity"];
      upperCaseStr = [array lastObject];
      }

      if ([[[dictsub valueForKey:@"airseg"]valueForKey:@"tocity"] isKindOfClass:[NSString class]]) {

      upperCaseStr = [NSString stringWithFormat:@"%@",[[dictsub valueForKey:@"airseg"]valueForKey:@"fromcity"]];

      }



      NSArray *commaSeprateArray = [upperCaseStr componentsSeparatedByString:@","];
      NSString *fromCityStr = [NSString stringWithFormat:@"%@ (%@)",[commaSeprateArray firstObject],[[dictsub valueForKey:@"airseg"]valueForKey:@"fromcitycode"]];



      cell.Airlinelbl.text = [NSString stringWithFormat:@"%@ ",[[[dictsub valueForKey:@"airseg"]valueForKey:@"airline"]stringByAppendingString:flightno]].uppercaseString;
      //cell.Fromtlbl.text = [[dictsub valueForKey:@"airseg"]valueForKey:@"fromcity"];

      if ([[[dictsub valueForKey:@"airseg"]valueForKey:@"tocity"] isKindOfClass:[NSArray class]])
      {
      NSArray *array = [[dictsub valueForKey:@"airseg"]valueForKey:@"tocity"];
      NSString *upperCaseStrr = [array lastObject];
      NSArray *commaSeprateArray1 = [upperCaseStrr componentsSeparatedByString:@","];
      NSString *fromCityStr1 = [NSString stringWithFormat:@"%@ (%@)",[commaSeprateArray1 firstObject],[[dictsub valueForKey:@"airseg"]valueForKey:@"tocitycode"]];
      cell.Fromtlbl.text = [NSString stringWithFormat:@"%@ - %@", fromCityStr,fromCityStr1];

      }
      if ([[[dictsub valueForKey:@"airseg"]valueForKey:@"tocity"] isKindOfClass:[NSString class]]) {

      NSString *cityStr = [[dictsub valueForKey:@"airseg"]valueForKey:@"tocity"];
      NSString *upperCaseStrr = cityStr;

      NSArray *commaSeprateArray1 = [upperCaseStrr componentsSeparatedByString:@","];
      NSString *fromCityStr1 = [NSString stringWithFormat:@"%@ (%@)",[commaSeprateArray1 firstObject],[[dictsub valueForKey:@"airseg"]valueForKey:@"tocitycode"]];
      cell.Fromtlbl.text = [NSString stringWithFormat:@"%@ - %@", fromCityStr,fromCityStr1];
      }
      // NSString *upperCaseStrr = [NSString stringWithFormat:@"%@",[[dictsub valueForKey:@"airseg"]valueForKey:@"tocity"]];

      // NSArray *commaSeprateArray1 = [upperCaseStrr componentsSeparatedByString:@","];
      // NSString *fromCityStr1 = [NSString stringWithFormat:@"%@ (%@)",[commaSeprateArray1 firstObject],[[dictsub valueForKey:@"airseg"]valueForKey:@"tocitycode"]];


      // cell.Fromtlbl.text = [NSString stringWithFormat:@"%@ - %@", fromCityStr,fromCityStr1];




      // cell.tolbl.text = [[dictsub valueForKey:@"airseg"]valueForKey:@"tocity"];
      cell.tripimage.image = [UIImage imageNamed:@"flight_large.png"];
      cell.conformationlbl.text = [NSString stringWithFormat:@"Confirmation #%@", [[dictsub valueForKey:@"airseg"]valueForKey:@"confirmation"]];

      }
      else if([dictsub valueForKey:@"carseg"]!= nil)
      {

      // cell.uparlineimageview.hidden = NO;
      // cell.lowerlineimgeview.hidden = NO;
      cell.timeLbl.text = [[dictsub valueForKey:@"carseg"]valueForKey:@"starttime"];

      cell.Airlinelbl.text = [NSString stringWithFormat:@"%@",[[dictsub valueForKey:@"carseg"]valueForKey:@"carvendor"]].uppercaseString;



      if ([[[dictsub valueForKey:@"carseg"]valueForKey:@"city"] isKindOfClass:[NSArray class]])
      {
      NSArray *array = [[dictsub valueForKey:@"carseg"]valueForKey:@"city"];
      cell.Fromtlbl.text = [array lastObject];

      }
      if ([[[dictsub valueForKey:@"carseg"]valueForKey:@"city"] isKindOfClass:[NSString class]]) {

      NSString *cityStr = [[dictsub valueForKey:@"carseg"]valueForKey:@"city"];
      cell.Fromtlbl.text = cityStr;
      }

      cell.tripimage.image = [UIImage imageNamed:@"transfer_large.png"];
      cell.tolbl.hidden= YES;
      cell.Tolb.hidden= YES;
      cell.conformationlbl.text =[NSString stringWithFormat:@"Confirmation #%@",[[dictsub valueForKey:@"carseg"]valueForKey:@"confirmation"]];




      // cell.webCheckInBtn.accessibilityIdentifier = indexPath.section;





      }
      else if([dictsub valueForKey:@"hotelseg"]!= nil)
      {
      NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
      [dateFormat setDateFormat: @"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
      NSDate *dte = [dateFormat dateFromString: [[ dictsub valueForKey:@"hotelseg"]valueForKey:@"stdt_d"]];
      //Second Conversion
      [dateFormat setDateFormat: @"hh:mm a"];
      NSString *finalStr = [dateFormat stringFromDate:dte];
      cell.timeLbl.text = finalStr;
      // cell.uparlineimageview.hidden = NO;
      // cell.lowerlineimgeview.hidden = YES;

      if([[dictsub valueForKey:@"hotelseg"]valueForKey:@"propertyname"]) {

      cell.Airlinelbl.text = [NSString stringWithFormat:@"%@",[[dictsub valueForKey:@"hotelseg"]valueForKey:@"propertyname"]].uppercaseString;
      }
      else {

      cell.Airlinelbl.text = @"HOTEL";
      }




      if ([[[dictsub valueForKey:@"hotelseg"]valueForKey:@"city"] isKindOfClass:[NSArray class]])
      {
      NSArray *array = [[dictsub valueForKey:@"hotelseg"]valueForKey:@"city"];
      cell.Fromtlbl.text = [array lastObject];

      }
      if ([[[dictsub valueForKey:@"hotelseg"]valueForKey:@"city"] isKindOfClass:[NSString class]]) {

      NSString *cityStr = [[dictsub valueForKey:@"hotelseg"]valueForKey:@"city"];
      cell.Fromtlbl.text = cityStr;
      }


      cell.tripimage.image = [UIImage imageNamed:@"hotel_big.png"];
      cell.tolbl.hidden= YES;
      cell.Tolb.hidden= YES;
      cell.conformationlbl.text =[NSString stringWithFormat:@"Confirmation #%@",[[dictsub valueForKey:@"hotelseg"]valueForKey:@"confirmation"] ];
      }
      }
      return cell;
      }
            
        
        
        else {
        
        static NSString *CellIdentifier = @"TripCell";
        
        TripCell *cell = (TripCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[TripCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }

        
        cell.webCheckInBtn.hidden = YES;

      NSArray *dataArray = [dataArrayAccordingdate objectAtIndex:indexPath.section];
        NSDictionary *dic = dataArray[indexPath.row];
        
        
       // for (int i =0 ; i< [dataArray count]; i++)
            
          //  for (NSDictionary *dicts in dic)

        //{
           // NSDictionary *dict = [dataArray objectAtIndex:i];
            //NSDictionary *dictsubb = [dictin valueForKey:@"travelsegment"];
            NSDictionary *dictsub;
           
            if ([dic valueForKey:@"airseg"]!= nil)
                
            {
                dictsub = [dic valueForKey:@"airseg"];
                
                // cell.uparlineimageview.hidden = YES;
                // cell.lowerlineimgeview.hidden = NO;
                cell.timeLbl.text = [dictsub valueForKey:@"departuretime"];
                NSString *timeStr = [dictsub valueForKey:@"departuretime"];
                NSString *date = [dictsub valueForKey:@"departuredate"];
                
                // code for weblink Show /Hide
                
                NSArray *dateArrayObjects = [date componentsSeparatedByString:@","];
                NSString *dateStr = [dateArrayObjects lastObject];

                NSString *finaldateStr = [NSString stringWithFormat:@"%@ %@",dateStr,timeStr];


                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];

                NSDate *currentDate = [NSDate date];
                NSString *dateString = [formatter stringFromDate:currentDate];

                NSLog(@" datestr =%@",dateString);



                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
                NSDate *date1 = [dateFormatter dateFromString:finaldateStr];
                 NSDate *date2 = [dateFormatter dateFromString:dateString];

                NSTimeInterval distanceBetweenDates = [date1 timeIntervalSinceDate :date2 ];
                double secondsInAnHour = 3600;
                NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;

                NSLog(@"hoursBetweenDates: %ld", hoursBetweenDates);


                //if ((hoursBetweenDates <= 24) || ( hoursBetweenDates >=  -24)) {

                    if ( 0 < hoursBetweenDates   && hoursBetweenDates <= 24) {


                    cell.webCheckInBtn.hidden = NO;

                }



                [cell.webCheckInBtn addTarget:self action:@selector(webCheckBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                cell.webCheckInBtn.tag =  (indexPath.section * 1000) + indexPath.row;



                NSString * flightno =[NSString stringWithFormat:@" %@",[dictsub valueForKey:@"flightnumber"]];
                NSString *upperCaseStr = [NSString stringWithFormat:@"%@",[dictsub valueForKey:@"fromcitycode"]];
              //  NSArray *commaSeprateArray = [upperCaseStr componentsSeparatedByString:@","];
                NSString *fromCityStr = [NSString stringWithFormat:@"%@",[dictsub valueForKey:@"fromcitycode"]];



                cell.Airlinelbl.text = [NSString stringWithFormat:@"%@ ",[[dictsub valueForKey:@"airline"]stringByAppendingString:flightno]].uppercaseString;
                //cell.Fromtlbl.text = [[dictsub valueForKey:@"airseg"]valueForKey:@"fromcity"];
               // NSString *upperCaseStrr = [NSString stringWithFormat:@"%@",[dictsub valueForKey:@"tocity"]];

               // NSArray *commaSeprateArray1 = [upperCaseStrr componentsSeparatedByString:@","];
                NSString *fromCityStr1 = [NSString stringWithFormat:@"%@",[dictsub valueForKey:@"tocitycode"]];


                cell.Fromtlbl.text = [NSString stringWithFormat:@"%@ - %@", fromCityStr,fromCityStr1];




                // cell.tolbl.text = [[dictsub valueForKey:@"airseg"]valueForKey:@"tocity"];
                cell.tripimage.image = [UIImage imageNamed:@"flight_large.png"];
                cell.conformationlbl.text = [NSString stringWithFormat:@"Confirmation #%@", [dictsub valueForKey:@"confirmation"]];
                
            }
            else if([dic valueForKey:@"carseg"]!= nil)
            {
                dictsub = [dic valueForKey:@"carseg"];


                // cell.uparlineimageview.hidden = NO;
                // cell.lowerlineimgeview.hidden = NO;
                cell.timeLbl.text = [[dictsub valueForKey:@"carseg"]valueForKey:@"departuretime"];

                cell.Airlinelbl.text = [NSString stringWithFormat:@"%@",[dictsub valueForKey:@"carvendor"]].uppercaseString;

                NSString *cityStr = [dictsub valueForKey:@"city"];
                cell.Fromtlbl.text = cityStr;
                cell.tripimage.image = [UIImage imageNamed:@"transfer_large.png"];
                cell.tolbl.hidden= YES;
                cell.Tolb.hidden= YES;
                cell.conformationlbl.text =[NSString stringWithFormat:@"Confirmation #%@",[dictsub valueForKey:@"confirmation"]];




                // cell.webCheckInBtn.accessibilityIdentifier = indexPath.section;





            }
            else if([dic valueForKey:@"hotelseg"]!= nil)
            {
                
                dictsub = [dic valueForKey:@"hotelseg"];

                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat: @"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
                NSDate *dte = [dateFormat dateFromString: [dictsub valueForKey:@"stdt_d"]];
                //Second Conversion
                [dateFormat setDateFormat: @"hh:mm a"];
                NSString *finalStr = [dateFormat stringFromDate:dte];
                cell.timeLbl.text = finalStr;
                // cell.uparlineimageview.hidden = NO;
                // cell.lowerlineimgeview.hidden = YES;
                cell.Airlinelbl.text = [NSString stringWithFormat:@"%@",[dictsub valueForKey:@"propertyname"]].uppercaseString;

                NSString *cityStr = [dictsub valueForKey:@"city"] ;
                cell.Fromtlbl.text = cityStr;
                cell.tripimage.image = [UIImage imageNamed:@"hotel_large.png"];
                cell.tolbl.hidden= YES;
                cell.Tolb.hidden= YES;
                cell.conformationlbl.text =[NSString stringWithFormat:@"Confirmation #%@",[dictsub valueForKey:@"confirmation"] ];
            }
            
            return cell;

        }
        
   
}
    
    
    else if (tableView == _citytabel ) {


        static NSString *CellIdentifier = @"CityCell";
        CityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[CityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }

        NSDictionary *cityDict = cityarr[indexPath.row];

        cell.citylbl.text = [NSString stringWithFormat:@"%@",[cityDict  valueForKey:@"iso"]];

        return  cell;
    }
    
    
    else
    {
        static NSString *CellIdentifier = @"notificationcell";
        
        NotificationCell *cell = (NotificationCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        NSDictionary *pnrDict = NotificationArray[indexPath.row];
        
        
        cell.titlelb.text = [NSString stringWithFormat:@"%@",[[pnrDict valueForKey:@"detail"]valueForKey:@"notificationTitle"]];
        cell.datalb.text = [NSString stringWithFormat:@"%@",[[pnrDict valueForKey:@"detail"]valueForKey:@"notificationText"]];
        
        return cell;
    }
    
    
    return cell;
    
    
}
    
    
    
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView ==_mytriptbl) {
    
        if ([responseType isEqualToString:@"TL24"]) {
                         
                       NSString *str = [filterrr objectAtIndex:indexPath.section];
                       NSMutableArray *arrRows = [[NSMutableArray alloc]init];
                       for (int i =0 ;i < [listArray count]; i++)
                       {
                       NSDictionary *dictin = [listArray objectAtIndex:i];
                       NSString *str2 = [dictin valueForKey:@"formatteddate"];

                       if ([str isEqualToString:str2]) {
                       [arrRows addObject:[listArray objectAtIndex:i]];
                       }
                       }

                       [TravelConstantClass singleton].TripArray = arrRows[indexPath.row];

                       NSLog(@"pkey%@",[TravelConstantClass singleton].TripArray);

                       for (NSDictionary *dictsub in [[TravelConstantClass singleton].TripArray valueForKey:@"travelsegment"])
                       {
                       if ([dictsub valueForKey:@"airseg"]!= nil)
                       {

                       FlightViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"flightvc"];
                           vc.typeUserStr =@"TL24";
                       vc.headerImage = headerImg;

                       [self.navigationController pushViewController:vc animated:YES];
                       }
                       else if ([dictsub valueForKey:@"carseg"]!= nil)
                       {

                       CarViewController *vc1 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"carvc"];
                       vc1.headerImage = headerImg;
                           vc1.typeUserStr =@"TL24";

                       [self.navigationController pushViewController:vc1 animated:YES];


                       }
                       else if ([dictsub valueForKey:@"hotelseg"]!= nil)
                       {

                       HotelViewController *vc1 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"hotelvc"];
                       vc1.headerImage = headerImg;
                      vc1.typeUserStr =@"TL24";

                       [self.navigationController pushViewController:vc1 animated:YES];


                       }

                       }
                       }
        
        else {
        
        
        NSArray *dataArray = [dataArrayAccordingdate objectAtIndex:indexPath.section];
        //NSDictionary *dic = dataArray[indexPath.row];
    
    [TravelConstantClass singleton].TripArray = dataArray[indexPath.row];
    
    NSLog(@"pkey%@",[TravelConstantClass singleton].TripArray);
    
        
        NSDictionary *dictsub = [TravelConstantClass singleton].TripArray;
//    for (NSDictionary *dictsub in [TravelConstantClass singleton].TripArray)
//    {
        if ([dictsub valueForKey:@"airseg"]!= nil)
        {
            
            FlightViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"flightvc"];
            vc.headerImage = headerImg;
            vc.typeUserStr =@"safeharbor";


            [self.navigationController pushViewController:vc animated:YES];
        }
        else if ([dictsub valueForKey:@"carseg"]!= nil)
        {
            
            CarViewController *vc1 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"carvc"];
            vc1.headerImage = headerImg;
            vc1.typeUserStr =@"safeharbor";


            [self.navigationController pushViewController:vc1 animated:YES];
            
            
        }
        else if ([dictsub valueForKey:@"hotelseg"]!= nil)
        {
            
            HotelViewController *vc1 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"hotelvc"];
            vc1.headerImage = headerImg;
            vc1.typeUserStr =@"safeharbor";

            [self.navigationController pushViewController:vc1 animated:YES];
            
            
        }
        
    }
    }
    
    
    else if (tableView ==_citytabel) {
    
        NSDictionary *dataDict = cityarr[indexPath.row];
        
        [TravelConstantClass singleton].City = [dataDict valueForKey:@"iso"];
        
        NSLog(@"pkey%@",[TravelConstantClass singleton].City);
        //  CityinfomationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CityinfomationVC"];
        
        
        CityInfoBySearchVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"CityInfoBySearchVC"];
        
        
        // CityInfoBySearchVC *myView  = [[CityInfoBySearchVC alloc] initWithNibName:@"CityInfoBySearchVC" bundle:nil];
        myView.strCity = [dataDict valueForKey:@"iso"];
        [_searchbar resignFirstResponder];
        
        [self.navigationController pushViewController:myView animated:YES];
        _citytabel.hidden = YES;

        
    }
    else
    {
        
        NotifDetailsVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"notif"];
        //  vc.alertId = [dataDict valueForKey:@"pnr"];
        
        NSDictionary *dataDict = NotificationArray[indexPath.row];
        if ([[dataDict valueForKey:@"detail"] isKindOfClass:[NSDictionary class]]) {
            vc. detailDic = dataDict;
            [self.navigationController pushViewController:vc animated:YES];
        
    }
    }
}



// code for destination guide



-(void)GETCity

{
    //  dispatch_async(dispatch_get_main_queue(), ^{
    
    [SVProgressHUD showWithStatus:@"City List" maskType:SVProgressHUDMaskTypeBlack];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSURL *url = [NSURL URLWithString:@"https:api.arrivalguides.com/api/xml/GetAllDestinations?auth=057bbab0770837e2364596fb21d6cf0e35ac9e44&v=13"];
        NSData *xmlData = [[NSData alloc] initWithContentsOfURL:url];
        NSError *parseError = nil;
        
        //  [SVProgressHUD showWithStatus:@"City List" maskType:SVProgressHUDMaskTypeBlack];
        NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:xmlData error:&parseError];
        NSLog(@"%@", xmlDictionary);
        
        cityarr = [[xmlDictionary objectForKey:@"destinations" ]objectForKey:@"destination"];
        // NSLog(@"%@",filterarr);
        finalData = [[xmlDictionary objectForKey:@"destinations" ]objectForKey:@"destination"];
        fillerarr = [finalData valueForKey:@"iso"];
        // NSLog(@"%@",arr);
        // dispatch_async(dispatch_get_main_queue(), ^{
        [_citytabel reloadData];
        [SVProgressHUD dismiss];
        
        
        if ([[NSString stringWithFormat:@"%@", [[[xmlDictionary valueForKey:@"Errors"] valueForKey:@"ErrorDescription"] objectAtIndex:0]] isEqualToString:@"No Data Found!"]) {
            //xmlDictionary = nil;
        }
        
        
    });
    
    
    
}
#pragma mark - Search Bar
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    _citytabel.hidden = NO;
    if([searchText length]>0){
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"iso CONTAINS[cd] %@",searchText];
        // searchBar.showsCancelButton=TRUE;
        NSArray *arraySearchedData=[finalData filteredArrayUsingPredicate:predicate];
        NSLog( @"searchdata%@",arraySearchedData);
        cityarr=[[NSMutableArray alloc]initWithArray:arraySearchedData];
        
        [_citytabel reloadData];
        
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    cityarr=[[NSMutableArray alloc]initWithArray:finalData];
    NSLog( @"city%@",cityarr);
    NSLog( @"Final%@",finalData);
    
    [_citytabel reloadData];
    
    [searchBar resignFirstResponder];
    _citytabel.hidden = YES;
    
}


#pragma mark - CityScrollview

-(void)loadpopulatCityonScrollview {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
     CGFloat screenHeight = screenRect.size.height;
    
    
    UIScrollView *scroll;
    if (screenHeight >= 812) {
           scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 360, screenWidth, screenHeight - 360 )];
        _popularLbl.frame = CGRectMake(20, 328, 300, 27 );
         _underlineLbl.frame = CGRectMake(20, 359, screenWidth-20, 1 );
        
}
       else {
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 350, screenWidth, screenHeight - 350 )];
           _popularLbl.frame = CGRectMake(20, 318, 300, 27 );
           _underlineLbl.frame = CGRectMake(20, 349, screenWidth-20, 1 );
       }
    scroll.backgroundColor = [UIColor whiteColor];
    [_searchCityView addSubview:scroll];
    
    CGFloat buttonHeight = 135;
    CGFloat buttonWidth = screenWidth/2 -15;
    CGFloat yAxis = 0 ;
    int index = 0;
    
    for (int i = 0; i < 3; i++) {
        yAxis = yAxis  + 20 ;

        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button addTarget:self
                   action:@selector(popularCityBtnClick:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[cityarry objectAtIndex:index] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:[imageOfCityArr objectAtIndex:index]] forState:UIControlStateNormal];

        button.frame = CGRectMake(10.0, yAxis, buttonWidth, 135);
       
       
         [scroll addSubview:button];
        
        
        index = index +1;
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 addTarget:self
                   action:@selector(popularCityBtnClick:)
         forControlEvents:UIControlEventTouchUpInside];
        [button1 setTitle:[cityarry objectAtIndex:index] forState:UIControlStateNormal];
        button1.frame = CGRectMake(buttonWidth +20 , yAxis, buttonWidth, 135);
        button1.backgroundColor = [UIColor whiteColor];
        [button1 setImage:[UIImage imageNamed:[imageOfCityArr objectAtIndex:index]] forState:UIControlStateNormal];

      
        [scroll addSubview:button1];
        
        yAxis = yAxis + buttonHeight;
        
        index = index +1;

        
    }
    
    scroll.contentSize = CGSizeMake(screenWidth, 700);
}


- (IBAction)popularCityBtnClick:(id)sender  {
    
    UIButton* btn = (UIButton*)sender;
    
    // CityinfomationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CityinfomationVC"];
    // CityInfoBySearchVC *vc  = [[CityInfoBySearchVC alloc] initWithNibName:@"CityInfoBySearchVC" bundle:nil];
    CityInfoBySearchVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"CityInfoBySearchVC"];
    
    
    myView.strCity = btn.titleLabel.text;
    [self.navigationController pushViewController:myView animated:YES];
    
}








@end


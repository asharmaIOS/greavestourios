//
//  HomeViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 4/28/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "homeCell.h"
#import "FlightViewController.h"
#import "SVProgressHUD.h"
#import "TravelConstantClass.h"
#import "Reachability.h"

#import <Foundation/Foundation.h>
#import "TripViewController.h"
#import "ValidationAndJsonVC.h"
#import "DemoMessagesViewController.h"
#import "TripDetailVC.h"
#import <SDWebImage/SDWebImage.h>






@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
{
    NSArray *pnrarr;
     NSArray *WetuIdsArray;
    NSArray *DestinationCityArray;
    NSMutableDictionary *pnrdic;
    NSString *itdate;
    UIRefreshControl *refreshControl;
}

@property (atomic, strong)  UIImageView *bigImageView;

@property (weak, nonatomic) IBOutlet UIView *emptyview;
@property (weak, nonatomic) IBOutlet UILabel *NoTriplbl;
@property (strong, atomic) NSMutableArray *dataArray;
@property (strong, atomic) NSMutableDictionary *dataDic;
@property (strong, atomic) NSMutableDictionary *WetuIdDic;
@property (strong, atomic) NSMutableDictionary *WetuIdContentDic;

@property (strong, atomic) NSMutableArray *totalArrayPky;

@property (strong, atomic) NSMutableArray *contentArray;
@property (strong, atomic) NSMutableArray *wetuIdArrayData;

@property (strong, atomic) NSString *startdate;
@property (weak,nonatomic)IBOutlet UIBarButtonItem *barButton;
@property (weak, nonatomic) IBOutlet UITableView *triplistTabel;

@end

@implementation HomeViewController
@synthesize startdate;


static SEL extracted() {
    return @selector(revealToggle:);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    _dataArray= [[NSMutableArray alloc]init];
    _contentArray= [[NSMutableArray alloc]init];
    
    _wetuIdArrayData = [[NSMutableArray alloc]init];
    _WetuIdDic = [[NSMutableDictionary alloc]init];
    _WetuIdContentDic = [[NSMutableDictionary alloc]init];
   // _totalArrayPky = [[NSMutableArray alloc]init];
    pnrarr = [[NSArray alloc]init];
    pnrdic = [[NSMutableDictionary alloc]init];
    WetuIdsArray = [NSArray arrayWithObjects:@"CF8BFE90-27DF-49B0-AE5F-F832ACFA87E0",@"0ab6ee40-65dc-43cc-b4d4-fec40258dace",@"2dcf31e1-8c19-4521-9f69-1f42ec7d16aa", nil];
    self.parentViewController.navigationController.navigationBarHidden= YES;
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController)
    {
        _barButton.target = self.revealViewController;
        _barButton.action = extracted();
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [self.navigationController.navigationBar setHidden: NO];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:235.0/255.0 green:0.0/255.0 blue:123.0/255.0 alpha:1.0];
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
     textButton.tintColor = [UIColor whiteColor];
    
     [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];


    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc] init];
    textButton1.title = @"MY TRIPS";
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[_barButton, textButton1];;

    
    // For Referece the table by scolling
    
    [self getProfile];
   
    for (int i=0; i<WetuIdsArray.count; i++) {
        
        [self callWetuItenaryById:[WetuIdsArray objectAtIndex:i]];
        [self callWetuContentById :[WetuIdsArray objectAtIndex:i]];
    }
    
    
    NSLog(@"%@--%@",_WetuIdDic,[_WetuIdContentDic allKeys]);
//    [self callWetuItenaryById:@"CF8BFE90-27DF-49B0-AE5F-F832ACFA87E0"];
//    [self callWetuContentById :@"CF8BFE90-27DF-49B0-AE5F-F832ACFA87E0"];
    
     [self drawView];
    
    // [self setupLeftMenuButton];
  
    
  ///  self.triplistTabel.tableFooterView = [UIView new];

    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    CGFloat screenMedianheight = screenRect.size.height/2;
    
    
    
    if (scrollView.contentOffset.y < 0 ) {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
    }
    
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
        
        
    }
    
    
}


- (void)getProfile {
    
    NSDictionary *dictionary = @{@"action":@"read",@"travelerID":[TravelConstantClass singleton].travelerID};
    
    NSString *urlStr = @"UpdateTraveler";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    if (dataDic.allKeys != nil) {
        //  [SVProgressHUD dismiss];
        
        
        //NSLog(@"pnr%@",listArray);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [TravelConstantClass singleton].Agencycode = [dataDic valueForKey:@"agencyCode"];
            NSLog(@"agencycode%@",[TravelConstantClass singleton].Agencycode);
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:[dataDic valueForKey:@"accountNumber"]  forKey: @"account"];
            [prefs synchronize];
            
            //  [SVProgressHUD dismiss];
            
            
            
        });
        
        
     //   [self callBackgroundFunctionForLocaldatabase ];
        
        if ([[dataDic valueForKey:@"Success"] isEqualToString:@"false"]||[[dataDic valueForKey:@"Success"] isEqualToString:@"0"]) {
            
            [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
            
            
        }
        
        
    }
    
}



-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}

-(void)refreshData
{
    //Put your logic here
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    
    if (networkStatus == NotReachable) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Opps" message:@"Please check your internet" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        [refreshControl endRefreshing];
                                        [self.triplistTabel reloadData];
                                    }
                                    ]];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else {
        
       // [self getListPnr];
        
        
        [refreshControl endRefreshing];
   
        
    }
}


- (void)animateImages :(NSArray *)imageArray
{
static int count = 0;
//NSArray *animationImages = @[[UIImage imageNamed:@"images.jpeg"], [UIImage imageNamed:@"images (1).jpeg"]];
UIImage *image = [imageArray objectAtIndex:(count % [imageArray count])];

[UIView transitionWithView:_bigImageView
                  duration:20.0f // animation duration
                   options:UIViewAnimationOptionTransitionCrossDissolve
                animations:^{
                    _bigImageView.image = image;
                } completion:^(BOOL finished) {
                    [self animateImages:imageArray];
                    count++;
                }];
    
}



-(void)drawView {
    
 //   NSMutableArray *drawnWetuIdArray = [[NSMutableArray alloc]init ];
    

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 44, screenWidth-20, screenHeight)];
    scroll.backgroundColor = [UIColor clearColor];
    [self.view addSubview:scroll];
    
   // NSArray *cityTitlename = [NSArray arrayWithObjects:@"Boutique Places & Fort of Regel Rajasthan",@"Kashmir, Heaven on Earth", nil];
    
    int y=15;
    for (int i=0; i <_wetuIdArrayData.count; i++) {
        
       // NSArray *values = [_WetuIdContentDic allValues];
        
        NSDictionary *dic = [_contentArray objectAtIndex:i];
        
//        NSDictionary *dic ;
//        if ([values count] != 0) {
//            dic  = [values objectAtIndex:i]; }
//        [drawnWetuIdArray addObject:[[_WetuIdContentDic allKeys]objectAtIndex:i]];
        NSArray *contentdataArray = [dic valueForKey:@"pins"];
        NSMutableArray* cityArray = [[NSMutableArray alloc]init];
        NSArray* imageArray ;
        
        for (int i = 0; i<contentdataArray.count; i++) {
            
            if ([[[contentdataArray objectAtIndex:i]valueForKey:@"category"]isEqualToString:@""]) {
                
                [cityArray addObject: [[contentdataArray objectAtIndex:i]valueForKey:@"position"]];
            }
            
            else if ([[[contentdataArray objectAtIndex:i]valueForKey:@"category"]isEqualToString:@"Hotel"]){
                
                imageArray = [[contentdataArray objectAtIndex:i]valueForKeyPath:@"content.images.url"];
            }
        }
        
       // NSArray * cityArray = [dic valueForKeyPath:@"pins.position"];
        
        // for wetuID api Response
        
//        NSArray *idvalues = [_WetuIdDic allValues];
//
//        NSDictionary *dic1 ;
//        if ([idvalues count] != 0) {
//            dic1  = [idvalues objectAtIndex:i]; }
        NSDictionary *wetuIdDic = [_wetuIdArrayData objectAtIndex:i] ;
      
        
     
        
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(5, y , scroll.frame.size.width -10, 320)];
        backgroundView.backgroundColor = [UIColor whiteColor];
        backgroundView.layer.shadowRadius  = 1.0f;
       // backgroundView.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
        backgroundView.layer.shadowColor   = [UIColor grayColor].CGColor;
        backgroundView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
        backgroundView.layer.shadowOpacity = 0.9f;
        backgroundView.layer.masksToBounds = NO;
       
        backgroundView.userInteractionEnabled = YES;
        backgroundView.tag = i;
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapAction:)];
        
        tapGesture1.numberOfTapsRequired = 1;
        [tapGesture1 setDelegate:self];
        [backgroundView addGestureRecognizer:tapGesture1];
        
        
        
        UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.0f, 0);
        UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(backgroundView.bounds, shadowInsets)];
        backgroundView.layer.shadowPath    = shadowPath.CGPath;
      
        _bigImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, backgroundView.frame.size.width, 180)];
       
//        NSMutableArray *imageArrayOfUIImage =[[NSMutableArray alloc]init ];
//        for (int i =0; i<imageArray.count; i++) {
//
//            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[imageArray objectAtIndex:i]]];
//            UIImage *image = [UIImage imageWithData:data];
//
//            [imageArrayOfUIImage addObject:image];
//        }
//
//

        
       // UIImageView *animatedImageView = [[UIImageView alloc] init];
//        _bigImageView.animationImages = imageArrayOfUIImage;
//        _bigImageView.animationDuration = 20;
//
//        [_bigImageView setAnimationRepeatCount:0];
//        [_bigImageView startAnimating];
        

        
        [_bigImageView sd_setImageWithURL:[NSURL URLWithString:[imageArray firstObject]]
                            placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
               _bigImageView.contentMode = UIViewContentModeScaleToFill;
              // bigImageofTour.image = [UIImage imageNamed:@"Lisbon3x.png"];
               [backgroundView addSubview:_bigImageView];
        
        
        //   [self animateImages:imageArrayOfUIImage];
//        [bigImageofTour sd_setImageWithURL:[NSURL URLWithString:[imageArray firstObject]]
//                     placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
//        bigImageofTour.contentMode = UIViewContentModeScaleToFill;
//       // bigImageofTour.image = [UIImage imageNamed:@"Lisbon3x.png"];
//        [backgroundView addSubview:bigImageofTour];
        
        UILabel *cityTitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 190, backgroundView.frame.size.width, 25)];
        cityTitle.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
        cityTitle.textColor = [UIColor colorWithRed:235.0/255.0 green:0.0/255.0 blue:123.0/255.0 alpha:1.0];
        cityTitle.text = [wetuIdDic valueForKey:@"name"];
        [backgroundView addSubview:cityTitle];
        
        float xaxis = 0;
        float yaxis = 225;
        
        for (int j=0; j<cityArray.count; j++) {
            
            if (j<10) {
              
            
            if (j%5==0 && j>0) {
                
                yaxis = 250;
                xaxis=0;
                
            }
               else if (j<5 && j>0) {
                    yaxis = 225;
                    xaxis=xaxis+70;
                }
                else if (10>j && j>5) {
                    yaxis = 250;
                    xaxis=xaxis+70;
                }
                UILabel *cityname = [[UILabel alloc]initWithFrame:CGRectMake(xaxis+10, yaxis, 65, 20)];
                cityname.textAlignment = NSTextAlignmentCenter;
                cityname.layer.cornerRadius  = 10.0f;
                cityname.layer.masksToBounds = YES;
                cityname.backgroundColor =[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
                cityname.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
                cityname.textColor = [UIColor grayColor];
                cityname.text = [[cityArray objectAtIndex:j]valueForKey:@"destination"];
                [backgroundView addSubview:cityname];
            
        }
        
        }
      
        
        UIImageView *locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 286, 15, 15)];
        locationIcon.image = [UIImage imageNamed:@"locationgray.png"];
        [backgroundView addSubview:locationIcon];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat: @"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *dte = [dateFormat dateFromString: [wetuIdDic valueForKey:@"start_date"]];
        //Second Conversion
        [dateFormat setDateFormat: @"dd MMMM yyyy"];
        NSString *finalStr = [dateFormat stringFromDate:dte];
        
        
        
        UILabel *startDate = [[UILabel alloc]initWithFrame:CGRectMake(27, 280, 250, 25)];
        startDate.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
        startDate.textColor = [UIColor lightGrayColor];
        
        
        startDate.text = [NSString stringWithFormat:@"Trip starts on %@",finalStr];
        [backgroundView addSubview:startDate];
        
        
        UIImageView *halfmoonIcon = [[UIImageView alloc] initWithFrame:CGRectMake(backgroundView.frame.size.width -110, 286, 15, 15)];
        halfmoonIcon.image = [UIImage imageNamed:@"nights.png"];
        [backgroundView addSubview:halfmoonIcon];
        
        
        UILabel *nightLbl = [[UILabel alloc]initWithFrame:CGRectMake(backgroundView.frame.size.width -90, 280, 90, 25)];
        nightLbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
        nightLbl.textColor = [UIColor lightGrayColor];
        nightLbl.text = [NSString stringWithFormat:@"%d Nights",[[wetuIdDic valueForKey:@"days"]intValue]-1];;
        [backgroundView addSubview:nightLbl];
        
        
//        UIImageView *plusIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 305, 15, 15)];
//        plusIcon.image = [UIImage imageNamed:@"plus.png"];
//        [backgroundView addSubview:plusIcon];
//
//
//        UILabel *serviceLbl = [[UILabel alloc]initWithFrame:CGRectMake(27, 305, 300, 25)];
//        serviceLbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
//        serviceLbl.textColor = [UIColor lightGrayColor];
//        serviceLbl.text = @"Hotels, Transfers, Sightseeing";
//        [backgroundView addSubview:serviceLbl];
        
        
        
       
        y=y+335;
        
        [scroll addSubview:backgroundView];
        
    }
    
    scroll.contentSize = CGSizeMake(scroll.frame.size.width, screenHeight+200);
}

- (void)tapAction:(UITapGestureRecognizer *)tap
{
   
    UIView *view = tap.view; //cast pointer to the derived class if needed
 
    
    
    
    TripDetailVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"tripDetail"];
    myView.wetuIdDic = [_wetuIdArrayData objectAtIndex:view.tag];
    myView.wetuContentIdDic = [_contentArray objectAtIndex:view.tag];
    
    [self.navigationController pushViewController:myView animated:true];
}


-(void)callWetuItenaryById : (NSString *)wetuId {
    
    NSString *urlStr = [NSString stringWithFormat:@"https://wetu.com/API/Itinerary/V7/Get?id=%@",wetuId];

   // NSURL *url = [NSURL URLWithString:@"https://wetu.com/API/Itinerary/V7/Get?id=CF8BFE90-27DF-49B0-AE5F-F832ACFA87E0"];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:url];
    
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
    id object = [NSJSONSerialization
                 JSONObjectWithData:oResponseData
                 options:0
                 error:&error];
    
    if(error) { /* JSON was malformed, act appropriately here */ }
    
    if (object) {
        
        [_wetuIdArrayData addObject:object];
      //  [_WetuIdDic setObject:object forKey:wetuId];
    }
    
     NSLog(@"Dict===%@",object);
    
//    if([object isKindOfClass:[NSArray class]])
//    {
//        NSArray*results = object;
//
//        NSLog(@"Arrray count===%ld",results.count);
//
//    }
//    else if([object isKindOfClass:[NSDictionary class]])
//    {
//       _dataDic = object;
//
//        NSLog(@"Dict===%@",_dataDic);
//
//    }

    
}


-(void)callWetuContentById : (NSString *)wetuId {
    
    NSString *urlStr = [NSString stringWithFormat:@"https://wetu.com/API/Itinerary/V7/GetContent?id=%@&appKey=ZP7KXUDXZP9RVMOD",wetuId];
    NSURL *url = [NSURL URLWithString:urlStr];

  //  NSURL *url = [NSURL URLWithString:@"https://wetu.com/API/Itinerary/V7/GetContent?id=CF8BFE90-27DF-49B0-AE5F-F832ACFA87E0&appKey=ZP7KXUDXZP9RVMOD"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:url];
    
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    
   // NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
        NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];


    id object = [NSJSONSerialization
                 JSONObjectWithData:oResponseData
                 options:0
                 error:&error];
    
    if(error) { /* JSON was malformed, act appropriately here */ }
    
  
    
    if (object) {
        
        [_contentArray addObject:object];
        
      //  [_WetuIdContentDic setObject:object forKey:wetuId];
    }
    
    
    NSLog(@"DictContent////===%@",object);

//    if([object isKindOfClass:[NSArray class]])
//    {
//        NSArray*results = object;
//
//         NSLog(@"Arrray count===%ld",results.count);
//
//    }
//    else if([object isKindOfClass:[NSDictionary class]])
//    {
//        NSDictionary *results = object;
//
//        NSLog(@"Dict===%@",[results valueForKey:@"pins"]);
//        _contentdataArray = [results valueForKey:@"pins"];
//
//
//        DestinationCityArray = [results valueForKeyPath:@"pins.position"];
//       // NSLog(@"Projects names: %@", projectsNames);
//
//
//
//    }

 
    
}






@end

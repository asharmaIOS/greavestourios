//
//  HorizantalCell.h
//  Travel Leader
//
//  Created by Prashant Khatri on 28/09/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HorizantalCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelName;

@end

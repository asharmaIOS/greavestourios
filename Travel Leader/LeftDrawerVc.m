

//
//  LeftDrawerVc.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 5/4/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "LeftDrawerVc.h"
#import "FlightViewController.h"
#import "DemoMessagesViewController.h"
#import "AboutusViewController.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "ContactusViewController.h"
#import "ProfileVC.h"
#import "XMLReader.h"
#import "SearchDestinationVC.h"
#import "TravelConstantClass.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "NotificationVC.h"
#import "FlightHomeVC.h"



@interface LeftDrawerVc () <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *menutbl;

@end

@implementation LeftDrawerVc


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //Return no. of Sections
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //Return no. of rows in section
    return 6;// (menu.count);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int height1;
    
    height1=70;
    NSLog(@"Clicked");
    
    return height1; // Normal height
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UIImage *thumbnail;
    _menutbl.backgroundColor = [UIColor clearColor];
    _menutbl.opaque = NO;
    _menutbl.backgroundView = nil;
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:18.0];
    
    switch (indexPath.row) {
            
//        case 0:
//            cell.textLabel.text = @"Profile";
//            thumbnail = [UIImage imageNamed:@"profile_icon.png"];
//            break;
            
        case 0:
            cell.textLabel.text = @"My Trips";
            thumbnail = [UIImage imageNamed:@"map.png"];
            break;
            
        case 1:
            cell.textLabel.text = @"My Flights";
            thumbnail = [UIImage imageNamed:@"flights.png"];
            break;
            
        case 2:
            cell.textLabel.text = @"Live Chat";
            thumbnail = [UIImage imageNamed:@"chat.png"];
            break;
        case 3:
            cell.textLabel.text = @"About Greaves";
            thumbnail = [UIImage imageNamed:@"user.png"];
            break;
            
        case 4:
            cell.textLabel.text = @"Contact Us";
            thumbnail = [UIImage imageNamed:@"phone.png"];
            break;
        case 5:
            cell.textLabel.text = @"Notifications";
            thumbnail = [UIImage imageNamed:@"notification.png"];
            break;
//        case 6:
//            cell.textLabel.text = @"Destination Guide";
//            thumbnail = [UIImage imageNamed:@"destination_icon.png"];
//            break;
        default:
            break;
    }
    CGSize itemSize = CGSizeMake(25, 25);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [thumbnail drawInRect:imageRect];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    
    //cell.imageView.image = thumbnail ;

    UIGraphicsEndImageContext();
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    if (indexPath.row==0)
    {
//        [self.revealViewController revealToggleAnimated:NO];
//        ProfileVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"profilevc"];
//        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//        [self presentViewController:lNavC animated:YES completion:^{
//        }];
        
        
        SWRevealViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        
        [self.navigationController pushViewController:vc animated:YES];
        

    }
    
    
    if (indexPath.row==1)
    {
//
//              FlightHomeVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"flighthome"];
//               UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//
//                [self presentViewController:lNavC animated:NO completion:^{
//                }];
        
        FlightHomeVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"flighthome"];
            //   [self.navigationController.navigationBar setHidden: NO];
               self.parentViewController.navigationController.navigationBarHidden= NO;

              [self.navigationController pushViewController:myView animated:true];

    }
    
    if (indexPath.row==2)
    {
//        [self.revealViewController revealToggleAnimated:NO];
//        DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
//        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//        [self presentViewController:lNavC animated:YES completion:^{
//        }];
        
        DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
        
        [self.navigationController pushViewController:myView animated:true];
    }
    
    
    if (indexPath.row==3)
    {
//        [self.revealViewController revealToggleAnimated:NO];
//        AboutusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutvc"];
//        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//        [self presentViewController:lNavC animated:YES completion:^{
//        }];
        
        AboutusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutvc"];
        
        [self.navigationController pushViewController:myView animated:true];
        
    }
    if (indexPath.row==4)
    {
//        [self.revealViewController revealToggleAnimated:NO];
//        ContactusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"contactvc"];
//
//        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//        [self presentViewController:lNavC animated:YES completion:^{
//        }];
//
        ContactusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"contactvc"];
        
        [self.navigationController pushViewController:myView animated:true];
    }
    if (indexPath.row==5)
    {
        //        [self.revealViewController revealToggleAnimated:NO];
        //        ContactusViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"contactvc"];
        //
        //        UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
        //        [self presentViewController:lNavC animated:YES completion:^{
        //        }];
        //
        NotificationVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
        
        [self.navigationController pushViewController:myView animated:true];
    }
    
//    if (indexPath.row==6)
//    {
//
//        SearchDestinationVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDestinationVC"];
//
//        [self.navigationController pushViewController:myView animated:true];
//    }
    
        
        
        
    
}



- (IBAction)destinationbtn:(id)sender
{
//    SearchDestinationVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDestinationVC"];
//
//    UINavigationController* lNavC = [[UINavigationController alloc] initWithRootViewController:myView];
//    [self presentViewController:lNavC animated:YES completion:^{
//    }];
    
    SearchDestinationVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDestinationVC"];
    
    [self.navigationController pushViewController:myView animated:true];
}

- (IBAction)logoutbtn:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Logout" message:@"Are you sure you want to logout" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"travelerID"];
                                     [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"apitype"];
                                
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                    
                              //      [AppDelegate sharedAppDelegate].window.rootViewController = login;
                                    [self.navigationController pushViewController:login animated :NO];
                                    
                                    
                                }
                                ]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // action 2
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];

    
}
@end

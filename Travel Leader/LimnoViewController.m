//
//  LimnoViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "LimnoViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface LimnoViewController ()
@property (weak, nonatomic) IBOutlet UIView *passangerview;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barbutton;

@end

@implementation LimnoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // self.searchBtn.layer.cornerRadius = self.searchBtn.frame.size.width / 2;
   // self.searchBtn.clipsToBounds = YES;
    self.passangerview.layer.cornerRadius; //= self.searchBtn.frame.size.width / 2;
    self.passangerview.clipsToBounds = YES;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if(revealViewController)
    {
        _barbutton.target = self.revealViewController;
        _barbutton.action = @selector(revealToggle:);
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

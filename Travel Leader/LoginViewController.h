//
//  LoginViewController.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 4/28/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
- (IBAction)Loginbtn:(id)sender;
- (IBAction)Registerbtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailtxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordtxt;
@property (weak, nonatomic) IBOutlet UISwitch *switchBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@property (weak, nonatomic) IBOutlet UIView *viewlogin;


@end

//
//  MapVC.h
//  Travel Leader
//
//  Created by wFares Dev on 3/4/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MapVC : UIViewController

@property (strong, atomic) NSMutableDictionary *wetuContentIdDic;
@property (strong, atomic) NSMutableDictionary *wetuIdDic;

@end

NS_ASSUME_NONNULL_END

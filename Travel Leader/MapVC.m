//
//  MapVC.m
//  Travel Leader
//
//  Created by wFares Dev on 3/4/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import "MapVC.h"
#import <GoogleMaps/GoogleMaps.h>

@interface MapVC () <GMSMapViewDelegate>

@property (strong, nonatomic) GMSMapView *mapView;

@property (strong, nonatomic)  NSMutableArray *completeDataArray;
@property (strong, nonatomic)  NSMutableArray *letArray;
@property (strong, nonatomic)  NSMutableArray *longArray;
@end

@implementation MapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    _mapView = [[GMSMapView alloc]initWithFrame:CGRectMake(0, 48, 380, 450)];
//    [self.view addSubview:_mapView];
    
    _mapView.myLocationEnabled = YES;
     self.view = _mapView;
    
    NSArray *completePinArray = [_wetuContentIdDic valueForKey:@"pins"];
   
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[[[completePinArray firstObject] valueForKey:@"position"]valueForKey:@"latitude"] doubleValue]
                                                                        longitude:[[[[completePinArray firstObject] valueForKey:@"position"]valueForKey:@"longitude"] doubleValue]
                                                                                  zoom:5];
    _mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    
    _letArray = [[NSMutableArray alloc]init ];
    _longArray = [[NSMutableArray alloc]init ];

    [self setupLeftMenuButton];
  //  [self loadView];
    
    [self letLongRefinedataFromWetuId:_wetuIdDic];
    
    [self drawMarkersAccrodingTheirType];

}


-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 07, 07)];
    [backButton addTarget:self action:@selector(backButtonTapp) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Google Map"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                    action:@selector(backButtonTapp)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}



-(void)backButtonTapp {
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)letLongRefinedataFromWetuId:(NSDictionary *)dataDic {
    
   
    NSArray *routeAllpointsArray = [dataDic valueForKey:@"routes"] ;
    for (int j=0; j< routeAllpointsArray.count;j++) {
        
        NSMutableArray *lettArray = [[NSMutableArray alloc]init ];
        NSMutableArray *longiArray = [[NSMutableArray alloc]init ];
        
        if ([[[routeAllpointsArray objectAtIndex:j ]valueForKey:@"mode"] isEqualToString:@"Transfer"]) {
    
       NSString *routeStr = [[routeAllpointsArray objectAtIndex:j ]valueForKey:@"points"];
        if (routeStr.length!=0 ) {
    
        
    NSArray *completeArray = [routeStr componentsSeparatedByString:@";"];
    for (int i =0; i<completeArray.count; i++) {
    if (i%2==0) {
    [lettArray addObject:[completeArray objectAtIndex:i]];
        }
    else {
        [longiArray addObject:[completeArray objectAtIndex:i]];
        }
      }
 
       }
            NSDictionary *dataDict = [routeAllpointsArray objectAtIndex:j];
            [self polilineDraw:lettArray :longiArray:[UIColor orangeColor]:dataDict];
        }
        
    if ([[[routeAllpointsArray objectAtIndex:j ]valueForKey:@"mode"] isEqualToString:@"ScheduledFlight"]) {
            
             int idOfcitybystart = [[[routeAllpointsArray objectAtIndex:j ] valueForKey:@"start_content_entity_id"]intValue ];
             int idOfcitybyend = [[[routeAllpointsArray objectAtIndex:j ] valueForKey:@"end_content_entity_id"]intValue ];
        
        NSArray *pinsArray = [_wetuContentIdDic valueForKey:@"pins"];
        
        for (int k=0; k<pinsArray.count; k++) {
            
            if ([[pinsArray objectAtIndex:k]valueForKey:@"position"]) {
                
                int  destinationContectStr = [[[pinsArray objectAtIndex:k]valueForKey:@"map_object_id"] intValue];
                
                if (idOfcitybystart == destinationContectStr||idOfcitybyend == destinationContectStr) {
                    [lettArray addObject:[[[pinsArray objectAtIndex:k]valueForKey:@"position"]valueForKey:@"latitude"]];
                     [longiArray addObject:[[[pinsArray objectAtIndex:k]valueForKey:@"position"]valueForKey:@"longitude"]];

                }
                
            }
            
        }
                CLLocationCoordinate2D position1 = CLLocationCoordinate2DMake([[lettArray firstObject] doubleValue],[[longiArray firstObject] doubleValue]);
                CLLocationCoordinate2D position2 = CLLocationCoordinate2DMake([[lettArray lastObject] doubleValue],[[longiArray lastObject] doubleValue]);
                CLLocationCoordinate2D centerpoint =   [self findCenterPoint:position1 :position2];
        
                NSString *letStr = [NSString stringWithFormat:@"%.20f",centerpoint.latitude];
                NSString *longStr = [NSString stringWithFormat:@"%.20f",centerpoint.longitude];
               [lettArray insertObject:letStr atIndex:1];
               [longiArray insertObject:longStr atIndex:1];
        [self polilineDraw:lettArray :longiArray :[UIColor redColor]:nil];
        
        
         [self markerOnspecificCoordinates:centerpoint];
            
        }
    }
    
   
}

-(CLLocationCoordinate2D)findCenterPoint:(CLLocationCoordinate2D)_lo1 :(CLLocationCoordinate2D)_loc2 {
    CLLocationCoordinate2D center;

    double lon1 = _lo1.longitude * M_PI / 180;
    double lon2 = _loc2.longitude * M_PI / 180;

    double lat1 = _lo1.latitude * M_PI / 180;
    double lat2 = _loc2.latitude * M_PI / 180;

    double dLon = lon2 - lon1;

    double x = cos(lat2) * cos(dLon);
    double y = cos(lat2) * sin(dLon);

    double lat3 = atan2( sin(lat1) + sin(lat2), sqrt((cos(lat1) + x) * (cos(lat1) + x) + y * y) );
    double lon3 = lon1 + atan2(y, cos(lat1) + x);

    center.latitude  = lat3 * 180 / M_PI;
    center.longitude = lon3 * 180 / M_PI;

    return center;
}


-(void)markerOnspecificCoordinates:(CLLocationCoordinate2D)coordinates {
    
               GMSMarker *marker  = [GMSMarker markerWithPosition:coordinates];
                      marker.title = @"Scheduled Flight";
                    UIImage *markerImg =[UIImage imageNamed:@"plane.png"];
               
                      marker.icon = markerImg;
                      marker.map = _mapView;
    
}

-(void)drawMarkersAccrodingTheirType {
    
    NSMutableArray *markerArray = [[NSMutableArray alloc]init];
    NSArray *completePinArray = [_wetuContentIdDic valueForKey:@"pins"];
    int indexOfhotel = 0;
    
    
    for (int i =0; i<completePinArray.count; i++) {
        
        NSDictionary *dataDic = [completePinArray objectAtIndex:i];
        
        NSString *catogoryStr = [NSString stringWithFormat:@"%@",[dataDic valueForKey:@"type"]];
        
        
        if ([catogoryStr isEqualToString:@"Accommodation"]) {
            BOOL checkHotelStatus = [self destinatiOnContentIdForHotelStanderdOrNot:[[dataDic valueForKey:@"position"]valueForKey:@"destination_content_entity_id"]];
            
            if (checkHotelStatus) {
            
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake([[[dataDic valueForKey:@"position"]valueForKey:@"latitude"] doubleValue], [[[dataDic valueForKey:@"position"]valueForKey:@"longitude"] doubleValue]);
               GMSMarker *marker;    marker = [GMSMarker markerWithPosition:position];
                   marker.title = [dataDic valueForKey:@"name"];
                    marker.snippet= @"Hotel";
            
            UIImage *markerImg = [UIImage imageNamed:[self imageNameWithdestinationSequence:indexOfhotel]];
            
                   marker.icon = markerImg;
                   marker.map = _mapView;
            
          //  [markerArray addObject: marker];

            indexOfhotel++;
        }
        
        }
        if ([[dataDic valueForKey:@"type"] isEqualToString:@"None"]) {
        
        if ([[dataDic valueForKey:@"category"] isEqualToString:@"International Airport"] ||[[dataDic valueForKey:@"category"] isEqualToString:@"Airport"] ) {
            
            CLLocationCoordinate2D position1 = CLLocationCoordinate2DMake([[[dataDic valueForKey:@"position"]valueForKey:@"latitude"] doubleValue], [[[dataDic valueForKey:@"position"]valueForKey:@"longitude"] doubleValue]);
                      GMSMarker *marker;      marker = [GMSMarker markerWithPosition:position1];
                            marker.title = [dataDic valueForKey:@"name"];
                     UIImage *markerImg = [UIImage imageNamed:@"Airport.png"];
                           marker.snippet= [dataDic valueForKey:@"category"];
                     
                            marker.icon = markerImg;
                            marker.map = _mapView;
                     
                  //   [markerArray addObject: marker];

            
        }
    
        }
    }
}

-(BOOL)destinatiOnContentIdForHotelStanderdOrNot:(NSString *)iddestination {
    BOOL idDestination = false;
    NSString *iddestStr = [NSString stringWithFormat:@"%@",iddestination];
  
    NSArray *legsArray = [_wetuIdDic valueForKey:@"legs"] ;
    for (int j=0; j< legsArray.count;j++) {
        NSString * destinationStrid = [[legsArray objectAtIndex:j]valueForKey:@"destination_content_entity_id"];
        
        if ([iddestStr intValue] == [destinationStrid intValue]) {
            if ([[[legsArray objectAtIndex:j]valueForKey:@"type"] isEqualToString:@"Standard"]) {
                 idDestination = true;
                break;
            }
           
        }
        
    }
    return idDestination;
}



-(void)markerForPathMedian:(CLLocationCoordinate2D)coordinates :(NSDictionary *)dic {
    
               GMSMarker *marker  = [GMSMarker markerWithPosition:coordinates];
               marker.title = @"Transfer";
    
               double time = [[dic valueForKey:@"duration"] doubleValue];
               float distance = [[dic valueForKey:@"distance"] floatValue];
              float distanceinKm = distance/1000 ;
              NSString *snipptStr = [NSString stringWithFormat:@" Distance: %.2f km \n Duration: %@ hours",distanceinKm,[dic valueForKey:@"duration"] ];
                    marker.snippet = snipptStr;
                    UIImage *markerImg =[UIImage imageNamed:@"bus.png"];
               
                      marker.icon = markerImg;
                      marker.map = _mapView;
    
}

-(void)polilineDraw :(NSMutableArray*)lattArray :(NSMutableArray *)longArray :(UIColor *)color : (NSDictionary *)distanceInfo{
    
 
    GMSMutablePath *path = [GMSMutablePath path];

    // Orange denote to path
    if (color == [UIColor orangeColor]) {
        int centerindex = longArray.count/2;
        NSString *string1 = [lattArray objectAtIndex:centerindex];
        NSString *string2 = [longArray objectAtIndex:centerindex];
        
          CLLocationCoordinate2D position1 = CLLocationCoordinate2DMake([string1 doubleValue],[string2 doubleValue]);
        
        [self markerForPathMedian:position1 :distanceInfo];
    }
    
    else {
        
        
    }

    for (int i = 0; i <longArray.count; i++) {

        NSString *string1 = [lattArray objectAtIndex:i];
        NSString *string2 = [longArray objectAtIndex:i];
        [path addCoordinate:CLLocationCoordinate2DMake(string1.doubleValue,string2.doubleValue)];
    }

        GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
    
//    NSInteger aRedValue = arc4random()%255;
//    NSInteger aGreenValue = arc4random()%255;
//    NSInteger aBlueValue = arc4random()%255;
//
//    UIColor *randColor = [UIColor colorWithRed:aRedValue/255.0f green:aGreenValue/255.0f blue:aBlueValue/255.0f alpha:1.0f];
        rectangle.strokeColor = color;
        rectangle.map = _mapView;
        rectangle.strokeWidth = 4.0f;
    
 
}




-(NSString *)imageNameWithdestinationSequence :(int)indexingNo{
    NSString *str ;
    
    if (indexingNo==0) {
        str =@"A.png";
    }else if (indexingNo==1){
        str =@"B.png";
    }else if (indexingNo==2){
        str =@"C.png";
    }else if (indexingNo==3){
        str =@"D.png";
    }else if (indexingNo==4){
        str =@"E.png";
    }else if (indexingNo==5){
        str =@"F.png";
    }else if (indexingNo==6){
        str =@"G.png";
    }else if (indexingNo==7){
        str =@"H.png";
    }else if (indexingNo==8){
        str =@"I.png";
    }
    else if (indexingNo==9){
        str =@"J.png";
    }
    else if (indexingNo==10){
        str =@"K.png";
    }
    else if (indexingNo==11){
        str =@"L.png";
    }
    else if (indexingNo==12){
        str =@"M.png";
    }
    else if (indexingNo==13){
        str =@"N.png";
    }
    else if (indexingNo==14){
        str =@"O.png";
    }
    
    
    
    return str;
}
//- (void)loadView {
//  // Create a GMSCameraPosition that tells the map to display the
//  // coordinate -33.86,151.20 at zoom level 6.
//  GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
//                                                          longitude:151.20
//                                                               zoom:6];
//  GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
//  mapView.myLocationEnabled = YES;
//  self.view = mapView;
//
//  // Creates a marker in the center of the map.
//  GMSMarker *marker = [[GMSMarker alloc] init];
//  marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
//  marker.title = @"Sydney";
//  marker.snippet = @"Australia";
//  marker.map = mapView;
//}


@end

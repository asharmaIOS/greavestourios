//
//  NotifDetailsVC.h
//  Travel Leader
//
//  Created by Abhishek Sharma on 4/4/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotifDetailsVC : UIViewController


@property (nonatomic,weak)NSString *alertId;
@property (nonatomic,weak)NSDictionary *detailDic;

@end

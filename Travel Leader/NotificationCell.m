//
//  NotificationCell.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 3/27/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

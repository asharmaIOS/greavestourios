//
//  ProfileVC.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/22/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "ViewController.h"

@interface ProfileVC : ViewController


@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;

@property (weak, nonatomic) IBOutlet UITextField *companyTF;

@property (weak, nonatomic) IBOutlet UITextField *poisionTF;


@property (weak, nonatomic) IBOutlet UITextField *agencyTF;

@end

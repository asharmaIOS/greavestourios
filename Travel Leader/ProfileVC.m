//
//  ProfileVC.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 8/22/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "ProfileVC.h"
#import "LeftDrawerVc.h"
#import "HomeViewController.h"
#import "ValidationAndJsonVC.h"
#import "SVProgressHUD.h"
#import "TravelConstantClass.h"
#import "Reachability.h"
#import "DemoMessagesViewController.h"


@interface ProfileVC ()<UITextFieldDelegate>

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setHidden: NO];
    self.navigationController.navigationBar.barTintColor=[UIColor orangeColor];
    [self    setupLeftMenuButton];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        [ValidationAndJsonVC displayAlert:@"No internet connected!" HeaderMsg:@"OOPS"];

    }
    else {
    
    [self  updateProfile];
        
    }
    // Do any additional setup after loading the view.
}



- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupLeftMenuButton{
    // for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //Text
//    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
//    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
//    textButton.title = @"PROFILE";
//    // textButton.tintColor = [UIColor whiteColor];
//    textButton.tintColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItems = @[searchItem];
//    self.navigationItem.rightBarButtonItems = @[textButton];
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc] init];
    textButton1.title = @"PROFILE";
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}


-(void)backButton:(id)sender
{
   
    HomeViewController* home = nil;
    for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
        NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
        if([vc isKindOfClass:[HomeViewController class]]) {
            home = (HomeViewController*)vc;
        }
    }
    
    if (home == nil) {
       // home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        
        home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        
        [self.navigationController pushViewController:home animated:NO];
        // Do we need to push it into navigation controller?
    }
    
}



- (void)updateProfile {
    
    [SVProgressHUD showWithStatus:@"Your Profile..." maskType:SVProgressHUDMaskTypeBlack];
    NSDictionary *dictionary = @{@"action":@"read",@"travelerID":[TravelConstantClass singleton].travelerID};

    NSString *urlStr = @"UpdateTraveler";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _companyTF.text = [dataDic valueForKey :@"company"];
            _emailTF.text = [[dataDic valueForKey :@"name"]valueForKey:@"fullname"];
            _poisionTF.text = [dataDic valueForKey :@"email"];
            _phoneTF.text = [dataDic valueForKey :@"phone"];
            _agencyTF.text = [dataDic valueForKey:@"agencyCode"];
            
            
        });
        
        
            if ([[dataDic valueForKey:@"Success"] isEqualToString:@"false"]||[[dataDic valueForKey:@"Success"] isEqualToString:@"0"]) {
                
                [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
                
                
          }

        
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

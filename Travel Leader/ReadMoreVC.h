//
//  ReadMoreVC.h
//  Travel Leader
//
//  Created by Gurpreet's on 14/11/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadMoreVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *cityImage;

@property (weak, nonatomic) IBOutlet UILabel *adresslb;

@property (weak, nonatomic) IBOutlet UILabel *phonelb;

@property (weak, nonatomic) IBOutlet UILabel *openlb;
@property (weak, nonatomic) IBOutlet UILabel *detaillb;

@end

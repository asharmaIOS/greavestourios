//
//  ReadMoreVC.m
//  Travel Leader
//
//  Created by Gurpreet's on 14/11/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "ReadMoreVC.h"
#import "TravelConstantClass.h"


@interface ReadMoreVC ()

@end

@implementation ReadMoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _detaillb.text = [ TravelConstantClass singleton].descriptions;
    
    NSURL* url = [NSURL URLWithString:[TravelConstantClass singleton].url];
    NSLog(@"url%@",url);
    //   cell.imageview.layer.cornerRadius = 10;
    
    NSData   *data = [NSData dataWithContentsOfURL:url];
    _cityImage.image = [UIImage imageWithData:data];
    
    NSLog(@"imge%@",_cityImage.image);
    
    [self setupLeftMenuButton];
    
     // Do any additional setup after loading the view.
}
-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //Text
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    textButton.title = @"";
    // textButton.tintColor = [UIColor whiteColor];
    textButton.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton];
    
    //  self.navigationItem.leftBarButtonItem = @[searchItem];
}

-(void)backButtonTapp:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  SearchDestinationVC.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 9/19/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "SearchDestinationVC.h"
#import "XMLReader.h"
#import "TravelConstantClass.h"
#import "CityinfomationVC.h"
#import "SVProgressHUD.h"
#import "CityCell.h"
#import "CityInfoBySearchVC.h"
#import "HomeViewController.h"
#import "DemoMessagesViewController.h"




@interface SearchDestinationVC ()<UISearchBarDelegate,UISearchDisplayDelegate>
{
    NSMutableArray* cityarr;
    NSArray *finalData;
    NSArray *fillerarr;
    
    
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;

@property (weak, nonatomic) IBOutlet UITableView *citytabel;
@property (nonatomic, strong) NSMutableArray *searchResult;
@end

@implementation SearchDestinationVC

- (void)viewDidLoad {
   // _searchbar.delegate = self;
    
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden: NO];
    self.navigationController.navigationBar.barTintColor=[UIColor orangeColor];
    cityarr = [[NSMutableArray alloc]init];
     fillerarr = [[NSMutableArray alloc]init];
    
    [self setupLeftMenuButton];
    _citytabel.hidden = YES;
    [self GETCity];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    
    _citytabel.hidden = YES;
    _searchbar.text = @"";
    [_searchbar resignFirstResponder];
   
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    // [self GETCity];
    
}
#pragma mark - navigation bar
-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
//    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
//    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
//    textButton.title = @"SEARCH";
//    // textButton.tintColor = [UIColor whiteColor];
//    textButton.tintColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItems = @[searchItem];
//    self.navigationItem.rightBarButtonItems = @[textButton];
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc] init];
    textButton1.title = @"SEARCH";
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}


-(void)backButtonTapp:(id)sender {
    
   // [self dismissViewControllerAnimated:YES completion:nil] ;
    
    HomeViewController* home = nil;
    for(int vv=0; vv<[self.navigationController.viewControllers count]; ++vv) {
        NSObject *vc = [self.navigationController.viewControllers objectAtIndex:vv];
        if([vc isKindOfClass:[HomeViewController class]]) {
            home = (HomeViewController*)vc;
        }
    }
    
    if (home == nil) {
        // home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        
        home = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
        
        [self.navigationController pushViewController:home animated:NO];
        // Do we need to push it into navigation controller?
    }
    // [self dismissViewControllerAnimated:YES completion:nil] ;
    
    
}





-(void)GETCity

{
  //  dispatch_async(dispatch_get_main_queue(), ^{

     [SVProgressHUD showWithStatus:@"City List" maskType:SVProgressHUDMaskTypeBlack];
    dispatch_async(dispatch_get_main_queue(), ^{

    NSURL *url = [NSURL URLWithString:@"https:api.arrivalguides.com/api/xml/GetAllDestinations?auth=057bbab0770837e2364596fb21d6cf0e35ac9e44&v=13"];
    NSData *xmlData = [[NSData alloc] initWithContentsOfURL:url];
         NSError *parseError = nil;
    
            //  [SVProgressHUD showWithStatus:@"City List" maskType:SVProgressHUDMaskTypeBlack];
    NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:xmlData error:&parseError];
    NSLog(@"%@", xmlDictionary);
    
    cityarr = [[xmlDictionary objectForKey:@"destinations" ]objectForKey:@"destination"];
   // NSLog(@"%@",filterarr);
   finalData = [[xmlDictionary objectForKey:@"destinations" ]objectForKey:@"destination"];
        fillerarr = [finalData valueForKey:@"iso"];
   // NSLog(@"%@",arr);
   // dispatch_async(dispatch_get_main_queue(), ^{
        [_citytabel reloadData];\
        [SVProgressHUD dismiss];
        
        
        if ([[NSString stringWithFormat:@"%@", [[[xmlDictionary valueForKey:@"Errors"] valueForKey:@"ErrorDescription"] objectAtIndex:0]] isEqualToString:@"No Data Found!"]) {
            //xmlDictionary = nil;
        }
        
        
    });
    

    
}
#pragma mark - Search Bar
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    _citytabel.hidden = NO;
    if([searchText length]>0){
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"iso CONTAINS[cd] %@",searchText];
       // searchBar.showsCancelButton=TRUE;
        NSArray *arraySearchedData=[finalData filteredArrayUsingPredicate:predicate];
         NSLog( @"searchdata%@",arraySearchedData);
        cityarr=[[NSMutableArray alloc]initWithArray:arraySearchedData];
       
        [_citytabel reloadData];
        
}
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    cityarr=[[NSMutableArray alloc]initWithArray:finalData];
      NSLog( @"city%@",cityarr);
    NSLog( @"Final%@",finalData);
  
    [_citytabel reloadData];
   
    [searchBar resignFirstResponder];
    _citytabel.hidden = YES;
    
}

- (IBAction)popularCityBtnClick:(id)sender  {
    
    UIButton* btn = (UIButton*)sender;
    
   // CityinfomationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CityinfomationVC"];
   // CityInfoBySearchVC *vc  = [[CityInfoBySearchVC alloc] initWithNibName:@"CityInfoBySearchVC" bundle:nil];
    CityInfoBySearchVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"CityInfoBySearchVC"];

    
     myView.strCity = btn.titleLabel.text;
      [self.navigationController pushViewController:myView animated:YES];

}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cityarr.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CityCell";
    CityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *cityDict = cityarr[indexPath.row];
    
    cell.citylbl.text = [NSString stringWithFormat:@"%@",[cityDict  valueForKey:@"iso"]];
    
    return  cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    NSDictionary *dataDict = cityarr[indexPath.row];
    
    [TravelConstantClass singleton].City = [dataDict valueForKey:@"iso"];
    
   NSLog(@"pkey%@",[TravelConstantClass singleton].City);
     //  CityinfomationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CityinfomationVC"];
    
    
    CityInfoBySearchVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"CityInfoBySearchVC"];
    
    
   // CityInfoBySearchVC *myView  = [[CityInfoBySearchVC alloc] initWithNibName:@"CityInfoBySearchVC" bundle:nil];
    myView.strCity = [dataDict valueForKey:@"iso"];
    [_searchbar resignFirstResponder];

    [self.navigationController pushViewController:myView animated:YES];
    _citytabel.hidden = YES;

    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
}
@end

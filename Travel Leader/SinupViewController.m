//
//  SinupViewController.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "SinupViewController.h"
#import "SVProgressHUD.h"
#import "TravelConstantClass.h"
#import "SWRevealViewController.h"
#import "HomeViewController.h"
#import "ValidationAndJsonVC.h"


@interface SinupViewController ()

@end

@implementation SinupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self    setupLeftMenuButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupLeftMenuButton{
    // for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //Text
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] init];
    //  UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 15, 15)];
    textButton.title = @"";
    // textButton.tintColor = [UIColor whiteColor];
    textButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton];
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
}
-(void)backButton:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)textFieldDidBeginEditingMaintainPosition:(UITextField *)textField {
    
    [UIView animateWithDuration:0.50
                     animations:^{
                         CGRect frame= _scroll.frame;
                         frame.origin.y-=170;
                         _scroll.frame=frame;
                         
                     }];
    
}

- (IBAction)textFieldShouldReturnMaintainPosition:(UITextField *)textField {
    [UIView animateWithDuration:0.50
                     animations:^{
                         CGRect frame= _scroll.frame;
                         frame.origin.y+=170;
                         _scroll.frame=frame;
                         
                     }];
    
    
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [UIView animateWithDuration:0.5 animations:^{
            CGRect framePromo=_viewsinup.frame;
            framePromo.origin.y+=320;
            _viewsinup.frame=framePromo;
        }];
    }
    return [self isEditing];;
}

- (IBAction)donebutton:(id)sender
{
    
    if ([self SinupValidation])
    {
        [self SinInRequest];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        //self.view.userInteractionEnabled = NO;
        
        
        
    }
}
-(BOOL)SinupValidation
{
    UIAlertView *alert;
    NSString *regex1 =@"\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    
    if (_emailtxt.text == nil || [_emailtxt.text length] == 0 || [[_emailtxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Email Address." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return FALSE;
        
    }
    else if (![predicate1 evaluateWithObject:_emailtxt.text] == YES)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your valid Email Address." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return FALSE;
    }
    else
        if (_passwordtxt.text == nil || [_passwordtxt.text length] == 0 || [[_passwordtxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
        else if (_phonetxt.text == nil || [_phonetxt.text length] == 0 || [[_phonetxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Phone Number." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
        else if (_lastnametxt.text == nil || [_lastnametxt.text length] == 0 || [[_lastnametxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Last Name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
    
        else if (_firstnametxt.text == nil || [_firstnametxt.text length] == 0 || [[_firstnametxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your First Name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
    
        else if (_companytxt.text == nil || [_companytxt.text length] == 0 || [[_companytxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Company Name." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
    
        else if (_Accounttxt.text == nil || [_Accounttxt.text length] == 0 || [[_Accounttxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Agency Account Number." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
    
        else if (_agencytxt.text == nil || [_agencytxt.text length] == 0 || [[_agencytxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )
        {
            alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Please enter your Agency Code." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return FALSE;
        }
    
    return TRUE;
}


- (void)SinInRequest {
    
    [SVProgressHUD showWithStatus:@"Creating your Account.." maskType:SVProgressHUDMaskTypeBlack];
 NSDictionary *dictionary = @{@"action":@"create",@"name":@{@"first":_firstnametxt.text,@"last":_lastnametxt.text},@"email":_emailtxt.text,@"password":_passwordtxt.text,@"agencyCode":_agencytxt.text, @"accountNumber":_Accounttxt.text,@"detail":@{@"company":_companytxt.text,@"phone":_phonetxt.text}};
    NSString *urlStr = @"UpdateTraveler";
    NSDictionary *dataDic = [ValidationAndJsonVC GetParsingNewData:dictionary GetUrl:urlStr GetTimeinterval:30];
    if (dataDic.allKeys != nil) {
        [SVProgressHUD dismiss];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([dataDic valueForKey:@"fault"]!= nil) {
                if ([[dataDic valueForKey:@"fault"] isEqualToString:@"notFound"])
                {
                    [ValidationAndJsonVC displayAlert:@"User NotFound"  HeaderMsg:@"Attention"];

                }
                else{
                    [ValidationAndJsonVC displayAlert:[dataDic valueForKey:@"fault"] HeaderMsg:@"Attention"];
}
                }
            else{
                [TravelConstantClass singleton].travelerID = [dataDic valueForKey:@"travelerID"];
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                // saving an NSString
                [prefs setObject: [TravelConstantClass singleton].travelerID forKey: @"travelerID"];
                [prefs setObject: _Accounttxt.text forKey: @"Accountno"];
                [prefs synchronize];
                NSLog(@"user%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
                // [TravelConstantClass singleton].AccountNo =_Accounttxt.text ;
                
                
                SWRevealViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"swvc"];
                
                [self.navigationController pushViewController:vc animated:YES];
                
            }
            
            [SVProgressHUD dismiss];
            
        });
        
        
        
        if ([[dataDic valueForKey:@"Success"] isEqualToString:@"false"]||[[dataDic valueForKey:@"Success"] isEqualToString:@"0"]) {
            
            [ValidationAndJsonVC displayAlert:@"This function required internet connectivity. Please connect to the internet" HeaderMsg:@"OOPS"];
            
            
        }
        
        
    }
    
}



@end

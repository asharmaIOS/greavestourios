//
//  TravelConstantClass.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 4/28/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef void (^SuccessHandler)(id responseObject);
typedef void (^ErrorHandler)(NSError *error);
//NSString *const kRalewayBold = @"Raleway-Bold";

@interface TravelConstantClass : NSObject <NSURLSessionDelegate>

@property (nonatomic, strong) SuccessHandler successHandler;
@property (nonatomic, strong) ErrorHandler errorHandler;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, assign) BOOL isoffline;
@property (strong, atomic) NSString *familyOrUser;
@property (strong, atomic) NSString *familyPatientId;
@property (nonatomic, assign) BOOL paidFamilyMember;

//***********
@property (strong, atomic) NSString *AccountNo;
@property (strong, atomic) NSString *travelerID;
@property (strong, atomic) NSString *london;
@property (strong, atomic) NSString *pkey;
@property (strong, atomic) NSString *Count;
@property (strong, atomic) NSString *caseID;
@property (strong, atomic) NSString *indexId;
@property (strong, atomic) NSString *City;
@property (strong, atomic) NSString *Agencycode;
@property (strong, atomic) NSString *descriptions;
@property (strong, atomic) NSString *address;
@property (strong, atomic) NSString *email;
@property (strong, atomic) NSString *phone;
@property (strong, atomic) NSString *url;
@property (strong,atomic)NSString *  FirebaseToken;


@property (strong, atomic) NSString *citydetail;
@property (strong, atomic) NSString *shareDocText;
@property (strong, atomic) NSString *recivedtext;


@property (strong, atomic) NSString *index;
@property (strong, atomic) NSString *type;
@property (strong, atomic) NSString *indexcount;
@property (nonatomic,assign) NSInteger  messagecount;
@property (strong, nonatomic) NSDictionary * pnrlistdic;


@property (strong, atomic) NSArray *txtArr;


@property (strong, atomic) NSArray *indexcountarr;

//****************
@property (strong, nonatomic) NSMutableArray *indexListarr;
@property (strong, nonatomic) NSMutableArray *imageURLarr;
@property (strong, nonatomic) NSMutableArray *cityarr;;

@property (strong, nonatomic) NSArray *sideseen;
@property (strong, nonatomic) NSMutableArray *TripArray;


@property (nonatomic, assign) BOOL noGraphsAvailable;
@property (strong, atomic) NSMutableArray *graphDataArray;
@property (strong, atomic) NSMutableArray *graphTextArray;
+ (instancetype)singleton;

-(void)gettingProfilePicture:(NSString *)str;
- (BOOL)validateEmailWithString:(NSString*)checkString;
- (BOOL)validatePhone:(NSString *)phoneNumber;
- (NSString *)formatDate:(NSDate *)date;
- (NSString *)finalString:(NSString *)dateStr;

-(void)postData:(NSString *)url  headerFields:(NSArray *)keys addValues:(NSArray *)values success:(SuccessHandler)success fail:(ErrorHandler)failure;
- (NSMutableURLRequest *)requestForPostMethod:(NSString *)wedApiStr headerFields:(NSArray *)keys addValues:(NSArray *)values;


-(void)getRecord:(NSString *)url successHanlder:(SuccessHandler)handler errorHandler:(ErrorHandler)errorHandler;

@end

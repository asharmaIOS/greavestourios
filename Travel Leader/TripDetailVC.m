//
//  TripDetailVC.m
//  Travel Leader
//
//  Created by wFares Dev on 6/12/19.
//  Copyright © 2019 Gurpreet Singh. All rights reserved.
//

#import "TripDetailVC.h"
#import "DemoMessagesViewController.h"
#import "MapVC.h"
#import <SDWebImage/SDWebImage.h>
#import <QuartzCore/QuartzCore.h>

#import <WebKit/WebKit.h>
#import <SDWebImage/SDWebImage.h>
#import "TripViewDetailCell.h"
#import <GoogleMaps/GoogleMaps.h>


static int const kHeaderSectionTag = 6900;


@interface TripDetailVC ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,WKNavigationDelegate, WKUIDelegate>
{
    int sectiontag;
    NSMutableIndexSet *expandedSectionsForDay;
    NSMutableIndexSet *expandedSectionsForHotel;
    NSArray *cellheightdayArr;
     NSArray *cellheighthotelArr;
    UIButton *mapButton;

}

@property (nonatomic, retain) UIScrollView *imageScrollView;

@property (nonatomic, retain) UIPageControl * pageControl;

@property (assign) UITableViewHeaderFooterView *expandedSectionHeader;
@property (strong) NSArray *sectionItems;
@property (strong) NSArray *sectionNames;
//@property (strong, nonatomic)  UIButton *mapButton;
@property (strong, nonatomic)  UIScrollView *bgScrollView;
@property (strong, nonatomic)  UIView *bgmainView;
@property (strong, nonatomic)  NSMutableArray *segmentArrayOfButtons;
@property (strong, nonatomic)  NSMutableArray *completedaysArray;
@property (strong, nonatomic)  NSMutableArray *hotelArray;
@property (strong, nonatomic)  UITextView *aboutTextView;
@property (strong, nonatomic)  UITableView *tableView;

@end

@implementation TripDetailVC

- (void)viewDidLoad {
    
    
   
    if (!expandedSectionsForDay)
    {
        expandedSectionsForDay = [[NSMutableIndexSet alloc] init];
    }
    
    
    if (!expandedSectionsForHotel)
    {
        expandedSectionsForHotel = [[NSMutableIndexSet alloc] init];
    }
    
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    [self setupLeftMenuButton];
    
    _tableView.hidden = YES;
    _completedaysArray = [[NSMutableArray alloc] init];
    _hotelArray = [[NSMutableArray alloc] init];
    _segmentArrayOfButtons = [[NSMutableArray alloc] init];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:235.0/255.0 green:0.0/255.0 blue:123.0/255.0 alpha:1.0];
    
    
    _bgScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    _bgScrollView.delegate = self;
    [self.view addSubview:_bgScrollView];
    //_bgmainView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    
    _aboutTextView =[[UITextView alloc]initWithFrame:CGRectMake(10, 280, screenWidth-20, screenHeight-270) ];
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(10, 280, screenWidth-20, screenHeight-200) ];
    _tableView.delegate =self;
    _tableView.dataSource =self;
    
    [_bgScrollView addSubview:_aboutTextView];
    [_bgScrollView addSubview:_tableView];
    
    
    UINib *nib = [UINib nibWithNibName:@"TripViewDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"TripDetailCell"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.bgScrollView.showsVerticalScrollIndicator = NO;



    

    [self drawView];
    [self getdaysArray];
    [self gethotelArray];
    
    cellheightdayArr =[self heightOfdaysArray:_completedaysArray];
    cellheighthotelArr=  [self heightOfhotelArray:_hotelArray];
    
    sectiontag = 0 ;
    _aboutTextView.hidden = NO;
    _tableView.hidden = YES;

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
    
    
       CGRect screenRect = [[UIScreen mainScreen] bounds];
       CGFloat screenWidth = screenRect.size.width;
       CGFloat screenHeight = screenRect.size.height;
    
        mapButton  = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth - 90,screenHeight -90, 67, 67)];
       [mapButton addTarget:self action:@selector(mapButtonTap) forControlEvents:UIControlEventTouchUpInside];
       [mapButton setBackgroundImage:[UIImage imageNamed:@"map-icon.png"] forState:UIControlStateNormal];
       [[[[UIApplication sharedApplication] delegate]window] addSubview:mapButton];
    
}



-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 07, 07)];
    [backButton addTarget:self action:@selector(backButtonTapp) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Itinerary"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                    action:@selector(backButtonTapp)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}

-(void)backButtonTapp {
    
    [mapButton removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)mapButtonTap {
    [mapButton removeFromSuperview];

   MapVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
   
    myView.wetuIdDic = _wetuIdDic;
    myView.wetuContentIdDic = _wetuContentIdDic;
    [self.navigationController pushViewController:myView animated:true];


}


-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}



-(void)getdaysArray{
    
      NSArray *idArray = [_wetuIdDic valueForKey:@"legs"];
    
    for (int i=0; i< idArray.count; i++) {
        
        NSDictionary *dataDic = [idArray objectAtIndex:i];
        
        NSArray *daysArray = [dataDic valueForKey:@"days"];
        
        
        for (int j=0; j<daysArray.count; j++) {
            
            NSDictionary *dic = [daysArray objectAtIndex:j];
            
            
            [_completedaysArray addObject:dic];
            
        }
        
    }
    
    
       NSLog(@"completedaysArray count===%d",_completedaysArray.count);
}

-(void)gethotelArray{
    
    NSArray *idArray = [_wetuContentIdDic valueForKey:@"pins"];
    
    for (int i=0; i< idArray.count; i++) {
        
        NSDictionary *dataDic = [idArray objectAtIndex:i];
        
        NSString *typeStr = [dataDic valueForKey:@"type"];
        
        
        if ([typeStr isEqualToString:@"Accommodation"]) {
            
             [_hotelArray addObject:dataDic];
        }
        
        
    }
    
    
    NSLog(@"hotelArray count===%d",_hotelArray.count);
}












- (void)scrollViewDidScroll:(UIScrollView *)scrollView {


    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    CGFloat screenMedianheight = screenRect.size.height/2;

    scrollView.bounces = (scrollView.contentOffset.y > 64);
    
    
    CGFloat pageWidth = self.imageScrollView.frame.size.width;
    self.imageScrollView.contentInset = UIEdgeInsetsZero;

    // _imageScrollView.bounces = (_imageScrollView.contentOffset.x > 50);
    float fractionalPage = self.imageScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;


}


-(NSString *) stringByStrippingHTML:(NSString*)originalString {
    NSRange r;
    NSString *s = [originalString copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(void)drawView {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;

        NSArray *contentdataArray = [_wetuContentIdDic valueForKey:@"pins"];
        NSMutableArray* cityArray = [[NSMutableArray alloc]init];
        NSArray* imageArray ;
        
        for (int i = 0; i<contentdataArray.count; i++) {
            
            if ([[[contentdataArray objectAtIndex:i]valueForKey:@"category"]isEqualToString:@""]) {
                
                [cityArray addObject: [[contentdataArray objectAtIndex:i]valueForKey:@"position"]];
            }
            
            else if ([[[contentdataArray objectAtIndex:i]valueForKey:@"category"]isEqualToString:@"Hotel"]){
                
                imageArray = [[contentdataArray objectAtIndex:i]valueForKeyPath:@"content.images.url"];
            }
            else if ([[[contentdataArray objectAtIndex:i]valueForKey:@"category"]isEqualToString:@"Country"]) {
                
                
                NSString *aboutStr =  [[contentdataArray objectAtIndex:i]valueForKeyPath:@"content.general_description"];
                
                NSString *replaceHtmltag = [self stringByStrippingHTML:aboutStr];
                
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:replaceHtmltag];
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                paragraphStyle.lineSpacing = 5;
                NSDictionary *dict = @{NSFontAttributeName :[UIFont fontWithName:@"Roboto-Regular" size:16] ,NSParagraphStyleAttributeName : paragraphStyle };
                [attributedString addAttributes:dict range:NSMakeRange(0, [replaceHtmltag length])];
            
                
                _aboutTextView.attributedText =  attributedString ;
                
            }
            
            
        }
        
        // NSArray * cityArray = [dic valueForKeyPath:@"pins.position"];
        
        // for wetuID api Response
        
    
        NSDictionary *wetuIdDic = _wetuIdDic ;
        
        
    
        
        UIImageView *bigImageofTour = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _bgScrollView.frame.size.width, 220)];
        
        [bigImageofTour sd_setImageWithURL:[NSURL URLWithString:[imageArray firstObject]]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        bigImageofTour.contentMode = UIViewContentModeScaleToFill;
        // bigImageofTour.image = [UIImage imageNamed:@"Lisbon3x.png"];
         [_bgScrollView addSubview:bigImageofTour];
        
//        UILabel *cityTitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 220,_bgScrollView.frame.size.width, 25)];
//        cityTitle.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
//        cityTitle.textColor = [UIColor colorWithRed:235.0/255.0 green:0.0/255.0 blue:123.0/255.0 alpha:1.0];
//        cityTitle.text = [wetuIdDic valueForKey:@"name"];
//        [_bgScrollView addSubview:cityTitle];
//
//        float xaxis = 0;
//        float yaxis = 250;
//
//        for (int j=0; j<cityArray.count; j++) {
//
//            if (j<10) {
//
//
//                if (j%5==0 && j>0) {
//
//                    yaxis = 275;
//                    xaxis=0;
//
//                }
//                else if (j<5 && j>0) {
//                    yaxis = 250;
//                    xaxis=xaxis+70;
//                }
//                else if (10>j && j>5) {
//                    yaxis = 275;
//                    xaxis=xaxis+70;
//                }
//                UILabel *cityname = [[UILabel alloc]initWithFrame:CGRectMake(xaxis+10, yaxis, 65, 20)];
//                cityname.textAlignment = NSTextAlignmentCenter;
//                cityname.layer.cornerRadius  = 10.0f;
//                cityname.layer.masksToBounds = YES;
//                cityname.backgroundColor =[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
//                cityname.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
//                cityname.textColor = [UIColor grayColor];
//                cityname.text = [[cityArray objectAtIndex:j]valueForKey:@"destination"];
//                [_bgScrollView addSubview:cityname];
//
//            }
//
//        }
//
//
//        UIImageView *locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 316, 15, 15)];
//        locationIcon.image = [UIImage imageNamed:@"location.png"];
//        [_bgScrollView addSubview:locationIcon];
//
//        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//        [dateFormat setDateFormat: @"yyyy-MM-dd'T'HH:mm:ssZ"];
//        NSDate *dte = [dateFormat dateFromString: [wetuIdDic valueForKey:@"start_date"]];
//        //Second Conversion
//        [dateFormat setDateFormat: @"dd MMMM yyyy"];
//        NSString *finalStr = [dateFormat stringFromDate:dte];
//
//
//
//        UILabel *startDate = [[UILabel alloc]initWithFrame:CGRectMake(27, 310, 250, 25)];
//        startDate.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
//        startDate.textColor = [UIColor lightGrayColor];
//
//
//        startDate.text = [NSString stringWithFormat:@"Trip starts on %@",finalStr];
//        [_bgScrollView addSubview:startDate];
//
//
//        UIImageView *halfmoonIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_bgScrollView.frame.size.width -110, 316, 15, 15)];
//        halfmoonIcon.image = [UIImage imageNamed:@"nights.png"];
//        [_bgScrollView addSubview:halfmoonIcon];
//
//
//        UILabel *nightLbl = [[UILabel alloc]initWithFrame:CGRectMake(_bgScrollView.frame.size.width -90, 310, 90, 25)];
//        nightLbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
//        nightLbl.textColor = [UIColor lightGrayColor];
//        nightLbl.text = [NSString stringWithFormat:@"%d Nights",[[wetuIdDic valueForKey:@"days"]intValue]-1];;
//        [_bgScrollView addSubview:nightLbl];
//
//
    // creating buttons for about
    
    UIView *buttonBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 220, screenWidth, 48)];
    
    
     buttonBgView.layer.borderWidth  = 2.0;
     buttonBgView.layer.borderColor  = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0].CGColor;
     buttonBgView.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
      [_bgScrollView addSubview:buttonBgView];
    
    CGFloat finalWidth = 0;

    NSArray *buttonName = [[NSArray alloc]initWithObjects:@"About This \nTour",@"Day by day \nitinerary",@"Hotel \nDetails", nil];
    for (int i = 0; i <3 ; i++) {
        
        CGFloat buttonWidth = screenWidth /3;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.tag = i;
        
        [button addTarget:self
                   action:@selector(segBtnTapped:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[buttonName objectAtIndex:i] forState:UIControlStateNormal];
        
        [button setFrame:CGRectMake(finalWidth,05, buttonWidth , 35)];
        
            button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            button.titleLabel.font = [UIFont systemFontOfSize:15];
            
        
        [button setTitleColor:[UIColor colorWithRed:80.0/255.0 green:80.0/255.0 blue:80.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        
        if (i==0) {
            
            [button setTitleColor:[UIColor colorWithRed:235.0/255.0 green:0.0/255.0 blue:123.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            
        }
        
        
        [buttonBgView addSubview:button];
        [_segmentArrayOfButtons addObject:button ];
        finalWidth = finalWidth + button.frame.size.width;
    }
    
    
    _bgScrollView.contentSize = CGSizeMake(_bgScrollView.frame.size.width, screenHeight+200);
}


- (IBAction)segBtnTapped:(id)sender {
    
    int index = [sender tag] ;
    
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    for (int i = 0; i<_segmentArrayOfButtons.count; i++) {
        
        if (i == index) {
            [[_segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor colorWithRed:235.0/255.0 green:0.0/255.0 blue:123.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        
        }
        else {
            [ [_segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor colorWithRed:80.0/255.0 green:80.0/255.0 blue:80.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        
        }
        
    }
    
    if(index==0){
        sectiontag = 0;

        _aboutTextView.hidden = NO;
        _tableView.hidden = YES;

    }
    else if(index==1){
         sectiontag = 1;

        _aboutTextView.hidden = YES;
        _tableView.hidden = NO;
        [_tableView reloadData];


    }
    else{
        sectiontag = 2;

       _aboutTextView.hidden = YES;
        _tableView.hidden = NO;
        [_tableView reloadData];

    }
    
        
        
    }
    



#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (sectiontag == 1) {
        
        return _completedaysArray.count;

    }
    else if (sectiontag ==2) {
        
           return _hotelArray.count;
    }
    else {
        
          return _completedaysArray.count;
        
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if(sectiontag ==1 ) {
        
        if ([expandedSectionsForDay containsIndex:section])
        {
            return 2; // return rows when expanded
        }
        
            
     }
        
        if (sectiontag ==2) {
        
      if ([expandedSectionsForHotel containsIndex:section]){
            
             return 2; // return rows when expanded
        }
            
    }
        
        return 1; // only top row showing
    }
    
    // Return the number of rows in the section.
    return 1;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"TripDetailCell";
    TripViewDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   
    
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            
           if (sectiontag ==1) {
               cell.daylbl.text = [NSString stringWithFormat:@"Day %ld",indexPath.section+1];
               NSString *cityStr = [self citytext:indexPath.section forstring:_completedaysArray];
                   if (cityStr) {
                            cell.Citylbl.text = cityStr;
                                 }
                   else if(indexPath.section == _completedaysArray.count-1){
                             cell.Citylbl.text = @"END OF ITINERARY";
                                  }
                                }
    
     else  if (sectiontag ==2) {
         
         cell.daylbl.text = [[_hotelArray objectAtIndex:indexPath.section]valueForKey:@"name"];
         cell.Citylbl.text = [[[_hotelArray objectAtIndex:indexPath.section]valueForKey:@"position"] valueForKey:@"area"];;
                                }
            
     else {
         
         cell.daylbl.text = [NSString stringWithFormat:@"Day %ld",indexPath.section+1];
         NSString *cityStr = [self citytext:indexPath.section forstring:_completedaysArray];
         
         if (cityStr) {
             cell.Citylbl.text = cityStr;
             
         }else if(indexPath.section == _completedaysArray.count-1){
             cell.Citylbl.text = @"END OF ITINERARY";
         }
        }
        
    }
    
      else {
          
              if (sectiontag == 1 ){
                  
              UITableViewCell * cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"kalal"] ;
              [ cell1.contentView addSubview:[self createSubviewForcellExpend:[_completedaysArray objectAtIndex:indexPath.section] :cell1.frame :[[cellheightdayArr objectAtIndex:indexPath.section]intValue]]];
                   return  cell1;
              }
          
          else if (sectiontag ==2 ){
             UITableViewCell * cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"kalal"] ;
            [ cell1.contentView addSubview:[self createSubviewForcellExpend:[_hotelArray objectAtIndex:indexPath.section]: cell1.frame:[[cellheighthotelArr objectAtIndex:indexPath.section]intValue]]];
              return  cell1;
              }
              
          }
        
        }
    
    else
    {
       cell.Citylbl.text = @"END OF  dfgfgd";
        
    }

    return  cell;
}


-(NSMutableArray *)heightOfdaysArray :(NSArray *)daysArray {
    NSMutableArray *cellheightFordaysCell = [[NSMutableArray alloc]init];
    
    
    for (int i=0; i<daysArray.count; i++) {
    
        NSDictionary *dataDictionary = [daysArray objectAtIndex:i];
        NSString *htmlStr = [NSString stringWithFormat:@"%@ %@ %@ %@",[dataDictionary valueForKey:@"notes"],[dataDictionary valueForKey:@"consultant_notes"],[dataDictionary valueForKey:@"included"],[dataDictionary valueForKey:@"excluded"]];
    
    
    NSString *htmlString = htmlStr;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding] options: @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error: nil];
    
    
    [attributedString addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:17.0],NSForegroundColorAttributeName:[UIColor blackColor]} range:NSMakeRange(0, attributedString.length)];
    CGFloat width = 364; // whatever your desired width is
    CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
        if (rect.size.height >1300) {
            
            [cellheightFordaysCell addObject: [NSNumber numberWithFloat:rect.size.height-60]];

            
        }
        else if (rect.size.height >1100) {
            
              [cellheightFordaysCell addObject: [NSNumber numberWithFloat:rect.size.height-50]];
        }
        
        else {
        [cellheightFordaysCell addObject: [NSNumber numberWithFloat:rect.size.height+80]];
            
        }
    }
    
    return cellheightFordaysCell;
}



-(NSMutableArray *)heightOfhotelArray:(NSArray *)hotelArray {
    NSMutableArray *cellheightForHotelCell = [[NSMutableArray alloc]init];
    
    
    for (int i=0; i<hotelArray.count; i++) {
        
        NSDictionary *dataDictionary = [hotelArray objectAtIndex:i];
        NSString *htmlStr = [NSString stringWithFormat:@"%@ ",[[dataDictionary valueForKey:@"content"]valueForKey:@"general_description"]];
        
        
        NSString *htmlString = htmlStr;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding] options: @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error: nil];
        
        
        [attributedString addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:16.0],NSForegroundColorAttributeName:[UIColor blackColor]} range:NSMakeRange(0, attributedString.length)];
        CGFloat width = 364; // whatever your desired width is
        CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        
        [cellheightForHotelCell addObject: [NSNumber numberWithFloat:rect.size.height+ 350]];
    }
    
    
    return cellheightForHotelCell;
}



-(UIView *)createSubviewForcellExpend :(NSDictionary *)dataDictionary :(CGRect)frameOfcell :(int )height{

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    UIView *finalView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth-20, height) ];
  
    if(sectiontag ==1) {
        
        CGFloat Yaxis = 0;

        
        if ([dataDictionary valueForKey:@"notes"]) {

            NSString *notesStr = [dataDictionary valueForKey:@"notes"];
            CGFloat heightOfNote = [self heightOfViewViaTheirText:notesStr];
            NSString *cssPath   = [[NSBundle mainBundle] pathForResource:@"style" ofType:@"css"];
            
          //  NSMutableString *htmlString = [NSMutableString stringWithFormat:@"<div style=\"font: 16pt/15pt Roboto-Regular;\">%@</div>", textFile ];

            
            NSData *cssData     = [NSData dataWithContentsOfFile:cssPath];
            NSString *cssString = [[NSString alloc] initWithData:cssData encoding:NSASCIIStringEncoding];
            WKWebView *webView = [[WKWebView alloc] init];
            webView.backgroundColor = [UIColor clearColor];
           // NSString *str = [notesStr stringByReplacingOccurrencesOfString:@"<p>"
           //                                             withString:@"<p class='p1'>"];
         //   NSString *finalhtmlString = [NSString stringWithFormat:@"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>%@</header>%@",cssString,str];

              NSString *finalhtmlString = [NSString stringWithFormat:@"<div style=\"font-size: 28pt; font-family: Arial;line-height:31pt;  margin:0\">%@</div>",notesStr];
            
            [webView loadHTMLString:finalhtmlString baseURL:nil];
            webView.UIDelegate = self;
            webView.frame = CGRectMake(0,Yaxis, screenWidth-20, heightOfNote);
            webView.scrollView.scrollEnabled = false;
            [finalView addSubview:webView];

            if (heightOfNote>600) {
                
                Yaxis = Yaxis +heightOfNote-85;

            }
            else {
                
                Yaxis = Yaxis +heightOfNote-50;

            }
        }


        if ([dataDictionary valueForKey:@"included"]) {

            NSString *includedStr = [dataDictionary valueForKey:@"included"];

           CGFloat heightOfNote = [self heightOfViewViaTheirText:includedStr];

            NSString *cssPath   = [[NSBundle mainBundle] pathForResource:@"style" ofType:@"css"];
            NSData *cssData     = [NSData dataWithContentsOfFile:cssPath];
            NSString *cssString = [[NSString alloc] initWithData:cssData encoding:NSASCIIStringEncoding];
            
            
            
            UILabel *includelbl = [[UILabel alloc]initWithFrame:CGRectMake(0,Yaxis+15, screenWidth-20, 30)];
            includelbl.text = @" Included";
            includelbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16.0];
            includelbl.backgroundColor = [UIColor colorWithRed:190.0/255.0 green:194.0/255.0 blue:198.0/255.0 alpha:1];
             [finalView addSubview: includelbl];
            Yaxis = Yaxis +45;
            
            WKWebView *webView = [[WKWebView alloc] init];
            webView.backgroundColor = [UIColor clearColor];
            NSString *str = [includedStr stringByReplacingOccurrencesOfString:@"<p>"
                                                        withString:@"<p class='p1'>"];
           // NSString *finalhtmlString = [NSString stringWithFormat:@"<header><meta name='viewport' content='width=device-width, initial-scale=0.8, maximum-scale=1.0, minimum-scale=1.0'>%@</header>%@",cssString,str];
            
            NSString *finalhtmlString = [NSString stringWithFormat:@"<div style=\"font-size: 28pt; font-family: Arial;line-height:31pt;  margin:0\">%@</div>",includedStr];


            [webView loadHTMLString:finalhtmlString baseURL:nil];
            webView.UIDelegate = self;
            webView.scrollView.scrollEnabled = false;
            webView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            webView.layer.borderWidth = 1.0f;
            webView.frame = CGRectMake(1,Yaxis, screenWidth-20, heightOfNote);

             [finalView addSubview: webView];
            Yaxis = Yaxis +heightOfNote+15;

       }

        if ([dataDictionary valueForKey:@"excluded"]) {

            NSString *excludedStr = [dataDictionary valueForKey:@"excluded"];

           CGFloat heightOfNote = [self heightOfViewViaTheirText:excludedStr];

            NSString *cssPath   = [[NSBundle mainBundle] pathForResource:@"style" ofType:@"css"];
            NSData *cssData     = [NSData dataWithContentsOfFile:cssPath];
            NSString *cssString = [[NSString alloc] initWithData:cssData encoding:NSASCIIStringEncoding];
            WKWebView *webView = [[WKWebView alloc] init];
            webView.backgroundColor = [UIColor clearColor];
            
            UILabel *includelbl = [[UILabel alloc]initWithFrame:CGRectMake(0,Yaxis, screenWidth-20, 30)];
                       includelbl.text = @" Excluded";
                       includelbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16.0];
                       includelbl.backgroundColor = [UIColor colorWithRed:190.0/255.0 green:194.0/255.0 blue:198.0/255.0 alpha:1];
                        [finalView addSubview: includelbl];
                       Yaxis = Yaxis +30;
            
            
            NSString *str = [excludedStr stringByReplacingOccurrencesOfString:@"<p>"
                                                        withString:@"<p class='p1'>"];
            
            NSString *finalhtmlString = [NSString stringWithFormat:@"<div style=\"font-size: 28pt; font-family: Arial;line-height:31pt;  margin:0\">%@</div>",excludedStr];

          //  NSString *finalhtmlString = [NSString stringWithFormat:@"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>%@</header>%@",cssString,str];

            [webView loadHTMLString:finalhtmlString baseURL:nil];
            webView.UIDelegate = self;
            webView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            webView.layer.borderWidth = 1.0f;
            webView.frame = CGRectMake(0,Yaxis, screenWidth-20, heightOfNote+15);
            webView.scrollView.scrollEnabled = false;

            [finalView addSubview:webView];


            Yaxis = Yaxis +heightOfNote +25 ;

        }

        if ([dataDictionary valueForKey:@"consultant_notes"]) {

            NSString *consultant_notesStr = [dataDictionary valueForKey:@"consultant_notes"];

           CGFloat heightOfNote = [self heightOfViewViaTheirText:consultant_notesStr];

            NSString *cssPath   = [[NSBundle mainBundle] pathForResource:@"style" ofType:@"css"];
            NSData *cssData     = [NSData dataWithContentsOfFile:cssPath];
            NSString *cssString = [[NSString alloc] initWithData:cssData encoding:NSASCIIStringEncoding];
            
            UILabel *includelbl = [[UILabel alloc]initWithFrame:CGRectMake(0,Yaxis, screenWidth-20, 30)];
            includelbl.text = @" Expert Tips";
            includelbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16.0];
            includelbl.backgroundColor = [UIColor colorWithRed:190.0/255.0 green:194.0/255.0 blue:198.0/255.0 alpha:1];
             [finalView addSubview: includelbl];
            Yaxis = Yaxis +30;
            
            WKWebView *webView = [[WKWebView alloc] init];
            webView.backgroundColor = [UIColor clearColor];
            NSString *str = [consultant_notesStr stringByReplacingOccurrencesOfString:@"<p>"
                                                        withString:@"<p class='p1'>"];
            
            NSString *finalhtmlString = [NSString stringWithFormat:@"<div style=\"font-size: 28pt; font-family: Arial;line-height:31pt;  margin:0\">%@</div>",consultant_notesStr];

            //NSString *finalhtmlString = [NSString stringWithFormat:@"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>%@</header>%@",cssString,str];

            [webView loadHTMLString:finalhtmlString baseURL:nil];
            webView.UIDelegate = self;
            webView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            webView.layer.borderWidth = 1.0f;
            webView.frame = CGRectMake(0,Yaxis, screenWidth-20, heightOfNote-10);
            webView.scrollView.scrollEnabled = false;

            [finalView addSubview:webView];


            Yaxis = Yaxis +heightOfNote-15;

        }




    
        
        
    }else if (sectiontag ==2) {
        
        finalView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth-20, height) ];

      _imageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth-20, 200)];
        _imageScrollView.backgroundColor = [UIColor clearColor];
        [_imageScrollView setShowsHorizontalScrollIndicator:NO];
        _imageScrollView.delegate = self;


           NSArray *hotelImageArray = [[dataDictionary valueForKey:@"content"]valueForKey:@"images"];
           
        for (int i =0; i<hotelImageArray.count; i++) {
            
 
            UIImageView *imageHotelView = [[UIImageView alloc]initWithFrame:CGRectMake(10+i*(screenWidth-20), 0, screenWidth-40, 200)];
            [imageHotelView sd_setImageWithURL:[NSURL URLWithString:[[hotelImageArray objectAtIndex:i]valueForKey:@"url"]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            [_imageScrollView addSubview:imageHotelView];
        }
        _imageScrollView.pagingEnabled = YES;
        
//        self.pageControl = [[UIPageControl alloc] init];
//        self.pageControl.frame = CGRectMake(screenWidth/2-60,200,120,30);
//        self.pageControl.numberOfPages = hotelImageArray.count;
//        self.pageControl.currentPage = 0;
//        _pageControl.pageIndicatorTintColor = [UIColor grayColor];
//        _pageControl.currentPageIndicatorTintColor = [UIColor redColor];
//
//        [self.pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
//
//        [finalView addSubview:self.pageControl];
        self.pageControl.backgroundColor = [UIColor clearColor];
        _imageScrollView.contentSize = CGSizeMake((screenWidth-20)*hotelImageArray.count, 200);

        [finalView addSubview:_imageScrollView];
        
        // your stay
                  UILabel *staylbl = [[UILabel alloc]initWithFrame:CGRectMake(0,210, screenWidth-20, 30)];
                   staylbl.text = @"  Your Stay";
                   staylbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16.0];
                   staylbl.backgroundColor = [UIColor colorWithRed:190.0/255.0 green:194.0/255.0 blue:198.0/255.0 alpha:1];
                    [finalView addSubview: staylbl];
        
        UILabel *nightlbl = [[UILabel alloc]initWithFrame:CGRectMake(0,240, screenWidth-20, 80)];
        nightlbl.numberOfLines =4;
        nightlbl.text = @"   3 Nights \n\n   Bed & Breakfast";
        nightlbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16.0];
        nightlbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
        nightlbl.layer.borderWidth = 1.0f;
         [finalView addSubview: nightlbl];
                   
        
        UILabel *roomlbl = [[UILabel alloc]initWithFrame:CGRectMake(0,330, screenWidth-20, 30)];
                          roomlbl.text = @"  Your Rooms";
                          roomlbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16.0];
                          roomlbl.backgroundColor = [UIColor colorWithRed:190.0/255.0 green:194.0/255.0 blue:198.0/255.0 alpha:1];
                           [finalView addSubview: roomlbl];
               
               UILabel *roomTypelbl = [[UILabel alloc]initWithFrame:CGRectMake(0,360, screenWidth-20, 35)];
               roomTypelbl.text = @"  1*Place Room";
               roomTypelbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16.0];
               roomTypelbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
               roomTypelbl.layer.borderWidth = 1.0f;
                [finalView addSubview: roomTypelbl];
        
        
        
        
        
        
        
        
        
        
        
        
         NSString *htmlStr = [NSString stringWithFormat:@"%@ ",[[dataDictionary valueForKey:@"content"]valueForKey:@"general_description"]];
     
        
        NSString *cssPath   = [[NSBundle mainBundle] pathForResource:@"style" ofType:@"css"];
        NSData *cssData     = [NSData dataWithContentsOfFile:cssPath];
        NSString *cssString = [[NSString alloc] initWithData:cssData encoding:NSASCIIStringEncoding];
        WKWebView *webView = [[WKWebView alloc] init];
            webView.backgroundColor = [UIColor clearColor];
            
            
            //  NSString *string = [dataDictionary valueForKey:@"notes"] ;
            NSString *str = [htmlStr stringByReplacingOccurrencesOfString:@"<p>"
                                                               withString:@"<p class='p1'>"];
        
        NSString *finalhtmlString = [NSString stringWithFormat:@"<div style=\"font-size: 28pt; font-family: Arial;line-height:31pt;  margin:0\">%@</div>",htmlStr];

         //   NSString *finalhtmlString = [NSString stringWithFormat:@"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>%@</header>%@",cssString,str];
            
            //   NSString *finalhtmlString = [NSString stringWithFormat:@"%@%@",cssString,str];
            
            [webView loadHTMLString:finalhtmlString baseURL:nil];
            webView.UIDelegate = self;
            webView.frame = CGRectMake(0,400, screenWidth-20, height-400);
            [finalView addSubview:webView];
            
        
        
    }
    
    
    return finalView;
}
// Page controll Methods


- (IBAction)changePage:(id)sender {
    CGFloat x = _pageControl.currentPage * _imageScrollView.frame.size.width;
    [_imageScrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView  {
    NSInteger pageNumber = roundf(scrollView.contentOffset.x / (scrollView.frame.size.width));
    _pageControl.currentPage = pageNumber;
}





-(CGFloat )heightOfViewViaTheirText :(NSString *)jsonParamForheight {
    
    CGFloat heightOfText = 0 ;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
       CGFloat screenWidth = screenRect.size.width;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData: [jsonParamForheight dataUsingEncoding:NSUnicodeStringEncoding] options: @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error: nil];
               [attributedString addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:16.0],NSForegroundColorAttributeName:[UIColor blackColor]} range:NSMakeRange(0, attributedString.length)];
               CGFloat width = screenWidth-20; // whatever your desired width is
               CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
               
               heightOfText = rect.size.height;
    
    return heightOfText;
}
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    // if (section>0) return YES;

    return YES;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            if(sectiontag ==1) {
                
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];

            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSectionsForDay containsIndex:section];
            NSInteger rows;


            NSMutableArray *tmpArray = [NSMutableArray array];

            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSectionsForDay removeIndex:section];

            }
            else
            {
                [expandedSectionsForDay addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }


            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }

            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationNone];

                UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UITableExpand"]];
                cell.accessoryView = imView;
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationNone];

                UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UITableContract"]];
                cell.accessoryView = imView;
            }
        }
            
            
            else if (sectiontag ==2) {
                
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                
                NSInteger section = indexPath.section;
                BOOL currentlyExpanded = [expandedSectionsForHotel containsIndex:section];
                NSInteger rows;
                
                
                NSMutableArray *tmpArray1 = [NSMutableArray array];
                
                if (currentlyExpanded)
                {
                    rows = [self tableView:tableView numberOfRowsInSection:section];
                    [expandedSectionsForHotel removeIndex:section];
                    
                }
                else
                {
                    [expandedSectionsForHotel addIndex:section];
                    rows = [self tableView:tableView numberOfRowsInSection:section];
                }
                
                
                for (int i=1; i<rows; i++)
                {
                    NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                                   inSection:section];
                    [tmpArray1 addObject:tmpIndexPath];
                }
                
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                
                if (currentlyExpanded)
                {
                    [tableView deleteRowsAtIndexPaths:tmpArray1
                                     withRowAnimation:UITableViewRowAnimationNone];
                    
                    UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UITableExpand"]];
                    cell.accessoryView = imView;
                }
                else
                {
                    [tableView insertRowsAtIndexPaths:tmpArray1
                                     withRowAnimation:UITableViewRowAnimationNone];
                    
                    UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UITableContract"]];
                    cell.accessoryView = imView;
                }
                
                
                
                
            }
    }

    NSLog(@"section :%d,row:%d",indexPath.section,indexPath.row);
    
   // [_tableView reloadData];
    }
}






- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row) {

            return 44.0f;

        }

        else {
            if (sectiontag ==1) {

                int height = [[cellheightdayArr objectAtIndex:indexPath.section] intValue];
                return height;

            }

            else if (sectiontag ==2){

                int height = [[cellheighthotelArr objectAtIndex:indexPath.section] intValue];

                return height;

            }else {

                return 44.0f;

            }

    }

    }
    else  {
        return 44.0f;

    }
    
 
}










-(NSString *)citytext:(int )index forstring :(NSArray *)completeArray {
    
    NSString *finalString;
    
    NSString *completeStr = [[completeArray objectAtIndex:index] valueForKey:@"notes"];
    
    if([completeStr containsString:@"</strong>"]) {
    
    NSString *listItems = [[completeStr componentsSeparatedByString:@"</strong>"] firstObject];
     finalString = [[listItems componentsSeparatedByString:@"<strong>"] lastObject];
    
    }
    
    else {
        
        finalString = nil;
    }

    return finalString;
}

@end

//
//  WebCheckInVC.h
//  Travel Leader
//
//  Created by Abhishek Sharma on 11/23/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebCheckInVC : UIViewController



@property (strong, atomic) NSString *webLink;

@end

//
//  WebCheckInVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 11/23/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//


#import <WebKit/WebKit.h>

#import "WebCheckInVC.h"

@interface WebCheckInVC () <WKNavigationDelegate, WKUIDelegate>


{
    
    UIActivityIndicatorView *activityIndicatorObject;
   
}
@property (weak, nonatomic) IBOutlet UIWebView *_webView;
@property(strong,nonatomic) WKWebView *webView;


@end

@implementation WebCheckInVC

- (void)viewDidLoad {
    
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItems = @[searchItem];;
    
    NSLog(@"webaddress ===%@",_webLink);
    
    //self.productURL = @"http://www.URL YOU WANT TO VIEW GOES HERE";
    if (_webLink) {
        
        
        activityIndicatorObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicatorObject.hidden = NO;
        [activityIndicatorObject startAnimating];
        activityIndicatorObject.center = self.view.center;
       
        [self.view addSubview:activityIndicatorObject];

       
    NSURL *url = [NSURL URLWithString:_webLink];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
        
    _webView = [[WKWebView alloc] initWithFrame:self.view.frame];
        _webView.backgroundColor = [UIColor clearColor];
    [_webView loadRequest:request];
        _webView.UIDelegate = self;
    _webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:_webView];
    
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    
    
    [activityIndicatorObject removeFromSuperview];
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    [activityIndicatorObject removeFromSuperview];
}
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
    
    
}

-(void)backButtonTapp:(id)sender {
    
    //    HomeViewController  *vc = [[UIStoryboard storyboardWithName:@"Main.storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"homevc"];
    //    [self.navigationController pushViewController:vc animated:YES];
    // [self dismissModalViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  main.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 3/31/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
